﻿import {parseStream} from 'fast-csv';
import {createReadStream} from 'fs'

import { Client } from './models/client';
import Contract from './models/contract';
import Claim from './models/claim';
import FinancialPlanner from './models/financialPlanner';
import Family from './models/family';

parseStream(
    createReadStream('./hanwha/TBL_CUST_DATA.csv'),
    { headers: true, trim: true })
    .on('data', client =>
        Client.create({
            id: client.CUST_ID,
            isFraud: client.SIU_CUST_YN == 'Y' ? true : false,
            sex: client.SEX == 1 ? true : false,
            age: client.AGE,
            costOfResd: client.RESI_COST,
            typecodeOfResd: client.RESI_TYPE_CODE,
            hasFPCareer: client.FP_CAREER == 'Y' ? true : false,
            dateRegistered: new Date(client.CUST_RGST / 100, client.CUST_RGST % 100),
            address1: client.CTPR,
            occpGrpCode1: client.OCCP_GRP1,
            occpGrpCode2: client.OCCP_GRP2,
            totalPrem: client.TOTALPREM == '' ? null : client.TOTALPREM,
            creditMin: client.MINCRDT == '' ? null : client.MINCRDT,
            creditMax: client.MAXCRDT == '' ? null : client.MAXCRDT,
            maritalStatus: client.WEDD_YN == 'Y' ? true : false,
            mateOccpGrpCode1: client.MATE_OCCP_GRP1,
            mateOccpGrpCode2: client.MATE_OCCP_GRP2,
            numOfChild: client.CHLD_CNT == '' ? null : client.CHLD_CNT,
            minChildAge: client.LTBN_CHLD_AGE ? client.LTBN_CHLD_AGE : null,
            ymMaxPayment: client.MAX_PAYM_YM == '' ?
                null : new Date(client.MAX_PAYM_YM / 100, client.MAX_PAYM_YM % 100),
            maxPayment: client.MAX_PRM == '' ?
                null : client.MAX_PRM,
            income: client.CUST_INCM == '' ? null : client.CUST_INCM,
            incomeRCBase: client.RCBASE_HSHD_INCM == '' ? null : client.RCBASE_HSHD_INCM,
            incomeJPBase: client.JPBASE_HSHD_INCM == '' ? null : client.JPBASE_HSHD_INCM
        }).catch(err => {
            throw err;
        })
    )
    .on('end', () => {
        parseStream(
            createReadStream('./hanwha/TBL_FMLY_DATA.csv'),
            { headers: true })
            .on('data', family =>
                Family.create({
                    relationCode: family.FMLY_RELN_CODE
                }).then(data => {
                    Client.findByPk(family.CUST_ID).then(client =>
                        data['setClient1'](client)).catch(err => {
                            throw err;
                        });
                    Client.findByPk(family.SUB_CUST_ID).then(client =>
                        data['setClient2'](client)).catch(err => {
                            throw err;
                        });
                }).catch(err => {
                    throw err;
                })
            )
            .on('end', () => console.log('done reading family data stream'));

        parseStream(
            createReadStream('./hanwha/TBL_FPINFO_DATA.csv'),
            { headers: true })
            .on('data', fp =>
                FinancialPlanner.create({
                    id: fp.CLLT_FP_PRNO,
                    statusOfPlanner: fp.INCB_DVSN,
                    dateEmployed: fp.ETRS_YM == '#' ?
                        null : new Date(fp.ETRS_YM / 100, fp.ETRS_YM % 100),
                    dateFired: fp.FIRE_YM == '#' || fp.FIRE_YM == 999912 ?
                        null : new Date(fp.FIRE_YM / 100, fp.FIRE_YM % 100),
                    previousJob: fp.BEFO_JOB,
                    education: fp.EDGB,
                    branchCode: fp.BRCH_CODE
                }).catch(err => { throw err; })
            )
            .on('end', () =>
                parseStream(
                    createReadStream('./hanwha/TBL_CNTT_DATA.csv'),
                    { headers: true })
                    .on('data', contract =>
                        Contract.create({
                            id: contract.POLY_NO,
                            clientRoleCode: contract.CUST_ROLE,
                            irkdCodeDtal: contract.IRKD_CODE_DTAL,
                            irkdCodeItem: contract.IRKD_CODE_ITEM,
                            categoryCode: contract.GOOD_CLSF_CDNM,
                            dateMade: new Date(contract.CNTT_YM / 100, contract.CNTT_YM % 100),
                            period: contract.REAL_PAYM_TERM,
                            channelCode: contract.SALE_CHNL_CODE,
                            statusCode: contract.CNTT_STAT_CODE,
                            dateOfExpiration: contract.EXPR_YM == 999912 ?
                                null : new Date(contract.EXPR_YM / 100, contract.EXPR_YM % 100),
                            dateExpired: contract.EXPR_YM ?
                                new Date(contract.EXTN_YM / 100, contract.EXTN_YM % 100) : null,
                            dateLost: contract.LAPS_YM ?
                                new Date(contract.LAPS_YM / 100, contract.LAPS_YM % 100) : null,
                            paymentPeriodCode: contract.PAYM_CYCL_CODE,
                            amountMain: contract.MAIN_INSR_AMT,
                            amountTotal: contract.SUM_ORIG_PREM,
                            daySpentPub: contract.RECP_PUBL,
                            daySpentContract: contract.CNTT_RECP,
                            annualIncome: contract.MNTH_INCM_AMT,
                            distance: contract.DISTANCE ? contract.DISTANCE : null
                        }).then(data => {
            Client.findByPk(contract.CUST_ID)
                .then(client => data['setClient'](client))
                .catch(err => { throw err; });
            FinancialPlanner.findByPk(contract.CLLT_FP_PRNO)
                .then(fp => data['setFinancialPlanner'](fp))
                .catch(err => { throw err; });
        }).catch(err => { throw err; })
                    )
                    .on('end', () =>
                        parseStream(
                        createReadStream('./hanwha/TBL_CLAIM_DATA.csv'),
                        { headers: true })
                        .on('data', claim =>
                            Claim.create({
                                id: claim.CNTT_RECP_SQNO,
                                occpGrpCode1: claim.ACCI_OCCP_GRP1,
                                occpGrpCode2: claim.ACCI_OCCP_GRP2,
                                fpChanged: claim.CHANG_FP_YN == 'Y' ?
                                    true : false,
                                dateAccident: parseDate(claim.RECP_DATE),
                                dateReasoningOriginal: parseDate(claim.ORIG_RESN_DATE),
                                dateReasoning: parseDate(claim.RESN_DATE),
                                currentStatus: claim.CRNT_PROG_DVSN,
                                typeOfAccident: claim.ACCI_DVSN,
                                causeOfAccident: claim.CAUS_CODE,
                                causeOfAccidentDetail: claim.CAUS_CODE_DTAL == '#' ?
                                    null : claim.CAUS_CODE_DTAL,
                                nameOfDisease: claim.DSAS_NAME,
                                treatmentStartDate: claim.HOSP_OTPA_STDT ?
                                    parseDate(claim.HOSP_OTPA_STDT) : null,
                                treatmentEndDate: claim.HOSP_OTPA_ENDT ?
                                    parseDate(claim.HOSP_OTPA_ENDT) : null,
                                resultCode: claim.RESL_CD1,
                                resultName: claim.RESL_NM1,
                                validTreatmentDays: claim.VLID_HOSP_OTDA,
                                distanceToHospital: claim.HOUSE_HOSP_DIST ? claim.HOUSE_HOSP_DIST : null,
                                codeOfHospital: claim.HOSP_CODE,
                                addrOfHospital: claim.ACCI_HOSP_ADDR ?
                                    claim.ACCI_HOSP_ADDR : null,
                                hospitalCategory: claim.HOSP_SPEC_DVSN,
                                licenseNum: claim.CHME_LICE_NO,
                                datePayment: claim.PAYM_DATE ?
                                    parseDate(claim.PAYM_DATE) : null,
                                costOfClaim: claim.DMND_AMT,
                                costGranted: claim.PAYM_AMT,
                                isActualCost: claim.PMMI_DLNG_YN == 'Y' ? true : false,
                                costSelfCharge: claim.SELF_CHAM ? claim.SELF_CHAM : null,
                                nonPayment: claim.NON_PAY ? claim.NON_PAY : null,
                                costSelfTotal: claim.TAMT_SFCA ? claim.TAMT_SFCA : null,
                                costPatientTotal: claim.PATT_CHRG_TOTA ? claim.PATT_CHRG_TOTA : null,
                                costDiscounted: claim.DSCT_AMT ? claim.DSCT_AMT : null,
                                numOfTreatment: claim.COUNT_TRMT_ITEM ? claim.COUNT_TRMT_ITEM : null,
                                ratioOfActual: claim.NON_PAY_RATIO,
                                isWarnedHospital: claim.HEED_HOSP_YN == 'Y' ? true : false
                            }).then(data => Contract.findByPk(claim.POLY_NO)
                                    .then(contract => data['setContract'](contract)))
                                .catch(err => { throw err; })
                        )
                            .on('end', () => console.log('done reading claim data stream'))
                    ));
    });

function parseDate(num) {
    return new Date(num / 10000, num % 10000 / 100, num % 100);
}