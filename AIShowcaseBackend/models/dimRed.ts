﻿import { DataTypes, Model, InferAttributes, InferCreationAttributes } from '@sequelize/core';
import sequelize from '../connect-db';

import {User} from './user';

export class DimRed extends Model<InferAttributes<DimRed>, InferCreationAttributes<DimRed>>{
    public id!: number;
    public tableName: string;
    public origin: string;
    public type: 'LDA svd' | 'LDA eigen' | 'PCA' | 'PCA rsvd';
    public label: string;
    public userId!: number;
}

DimRed.init({
    id: {
        type: DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        // originally this should be concatenated from date and appropriate number
        autoIncrement: true
    },
    tableName: {
        type: DataTypes.STRING, unique: true, allowNull: false
    },
    origin: { type: DataTypes.STRING, allowNull: false },
    type: {
        type: DataTypes.ENUM('LDA svd', 'LDA eigen', 'PCA', 'PCA rsvd'),
        allowNull: false
    },
    label: DataTypes.STRING,
    userId: {
        type: DataTypes.INTEGER.UNSIGNED
    }
}, {
    sequelize
});

DimRed.belongsTo(User);