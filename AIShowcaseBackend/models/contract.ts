﻿import { DataTypes } from '@sequelize/core';
import sequelize from '../connect-db';

import { Client } from './client'
import FinancialPlanner from './financialPlanner';

const Contract = sequelize.define('contract', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    clientRoleCode: DataTypes.INTEGER.UNSIGNED,
    irkdCodeDtal: DataTypes.INTEGER.UNSIGNED,
    irkeCodeItem: DataTypes.INTEGER.UNSIGNED,
    categoryCode: DataTypes.STRING,
    dateMade: DataTypes.DATEONLY,
    period: {
        type: DataTypes.INTEGER.UNSIGNED,
        validate: { max: 999 }
    },
    channelCode: {
        type: DataTypes.INTEGER.UNSIGNED,
        validate: { min: 1, max: 7 }
    },
    statusCode: DataTypes.STRING,
    dateOfExpiration: DataTypes.DATEONLY,
    dateExpired: DataTypes.DATEONLY,
    dateLost: DataTypes.DATEONLY,
    paymentPeriodCode: {
        type: DataTypes.INTEGER.UNSIGNED,
        validate: { max: 12 }
    },
    amountMain: DataTypes.INTEGER.UNSIGNED,
    amountTotal: DataTypes.INTEGER.UNSIGNED,
    daySpentPub: DataTypes.INTEGER.UNSIGNED,
    daySpentContract: DataTypes.INTEGER.UNSIGNED,
    annualIncome: DataTypes.INTEGER.UNSIGNED,
    distance: DataTypes.INTEGER.UNSIGNED
});

Contract.belongsTo(Client);
Contract.belongsTo(FinancialPlanner);

export default Contract;