﻿import { hash, compare } from 'bcrypt';
import { Model, DataTypes, InferAttributes, InferCreationAttributes } from '@sequelize/core';
import sequelize from '../connect-db';
const { Attribute, PrimaryKey, BeforeCreate,BeforeUpdate } = require('@sequelize/core/decorators-legacy');

const NUM_ROUNDS = 10;

export class User extends Model<InferAttributes<User>, InferCreationAttributes<User>> {
    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @PrimaryKey
    declare id: number;

    @Attribute(DataTypes.STRING)
    declare password: string;

    public email: string;
    public emailVerified: boolean;
    public nickName: string;
    public oauthId: string;
    public birthday: Date;
    public accountType: 'admin' | 'local' | 'google' | 'facebook';
    public hasCustomThumbnail: boolean;
    public dateVerifySent: Date;
    public dateSigned: Date;
    public tempId: number | null;

    public comparePassword(password:string, callback:any) {
        const user=this;
        compare(password, this.password, function (err, result) {
            if (err) {
                console.error(err);
                callback(null, false);
            }
            else if (!result)
                callback(null, false);
            else
                callback(null, user);
        });
    }

    @BeforeCreate
    public hashPassword() {
        const user = this;
        hash(user.password, NUM_ROUNDS).then(hashed => { user.update({ password: hashed }) })
            .catch(err => { throw err });
    }

    @BeforeUpdate
    public checkPassword() {
        const user = this;
        const changes = user.changed();
        if (!changes)
            return;
        if (changes.includes('password'))
            hash(user.password, NUM_ROUNDS, function (err, hashed) {
                if (err)
                    throw err;
                else {
                    user.password = hashed;
                }
            });
    }
}

User.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    email: {
        type: DataTypes.STRING,
        unique: true,
        validate: { isEmail: true }
    },
    emailVerified: { type: DataTypes.BOOLEAN, defaultValue: false },
    nickName: DataTypes.STRING,
    oauthId: DataTypes.STRING,
    password: DataTypes.STRING,
    birthday: {
        type: DataTypes.DATE,
        validate: { isBefore: '2000-01-01' }
    },
    accountType: {
        type: DataTypes.ENUM
    },
    hasCustomThumbnail: { type: DataTypes.BOOLEAN, defaultValue: false },
    dateVerifySent: DataTypes.DATE,
    dateSigned: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    tempId: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: null }
}, {
        sequelize,
        validate: {
            checkAccount () {
                if (this.accountType == 'local' || this.accountType == 'admin') {
                    if (this.oauthId) {
                        throw new Error('oAuth not null but type is local.');
                    }
                    else if (!this.email) {
                        throw new Error('local account must have a valid email address');
                    }
                }
                else {
                    if (!this.oauthId || !this.email)
                        throw new Error('Invalid OAuth2.0 account');
                }
                if (this.nickName)
                    if (this.nickName.indexOf(' ') > -1)
                        throw new Error('Nickname cannot have blank');
                return true;
            }
        }
});