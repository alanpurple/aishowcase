﻿import { DataTypes, InferAttributes, InferCreationAttributes, Model, CreationOptional } from '@sequelize/core';
const { Attribute, PrimaryKey, AutoIncrement, NotNull, Default } = require('@sequelize/core/decorators-legacy');
import { Max, Min } from '@sequelize/validator.js'

export class Client extends Model<InferAttributes<Client>, InferCreationAttributes<Client>> {
    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @PrimaryKey
    @AutoIncrement
    declare id: CreationOptional<number>;

    @Attribute(DataTypes.BOOLEAN)
    declare isFraud: boolean;

    @Attribute(DataTypes.BOOLEAN)
    declare sex: boolean;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Max(150)
    declare age: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costOfResd: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(11)
    @Max(99)
    declare typecodeOfResd: number;

    @Attribute(DataTypes.BOOLEAN)
    declare hasFPCareer: boolean;

    @Attribute(DataTypes.DATEONLY)
    declare dateRegistered: Date;

    @Attribute(DataTypes.STRING)
    declare address1: string;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(8)
    declare occpGrpCode1: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(25)
    declare occpGrpCode2: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare totalPrem: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Default(3)
    declare creditMin: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Default(3)
    declare creditMax: number;

    @Attribute(DataTypes.BOOLEAN)
    declare maritalStatus: boolean;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(8)
    declare mateOccpGrpCode1: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(25)
    declare mateOccpGrpCode2: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare numOfChild: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare minChildAge: number;

    @Attribute(DataTypes.DATEONLY)
    declare ymMaxPayment: Date;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare maxPayment: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare income: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare incomeRCBase: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare incomeJPBase: number;
};