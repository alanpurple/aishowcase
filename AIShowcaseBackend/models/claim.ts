﻿import { Model, DataTypes, InferAttributes, InferCreationAttributes,CreationOptional } from '@sequelize/core';
const { Attribute, PrimaryKey, AutoIncrement, NotNull, Default } = require('@sequelize/core/decorators-legacy');
import sequelize from '../connect-db';

import Contract from './contract';
import { Max, Min } from '@sequelize/validator.js';

export class Claim extends Model<InferAttributes<Claim>, InferCreationAttributes<Claim>> {
    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @PrimaryKey
    @AutoIncrement
    declare id: CreationOptional<number>;

    // occupation at the moment the accident happened
    @Attribute(DataTypes.STRING)
    declare occpGrpCode1: string;

    @Attribute(DataTypes.STRING)
    declare occpGrpCode2: string;

    @Attribute(DataTypes.BOOLEAN)
    @Default(false)
    declare fpChanged: boolean;

    @Attribute(DataTypes.DATEONLY)
    declare dateAccident: Date;

    @Attribute(DataTypes.DATEONLY)
    declare dateReasoningOriginal: Date;

    @Attribute(DataTypes.DATEONLY)
    declare dateReasoning: Date;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(11)
    @Max(33)
    declare currentStatus: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(3)
    declare typeOfAccident: number;

    @Attribute(DataTypes.STRING)
    declare causeOfAccident: string;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Max(200)
    declare causeOfAccidentDetail: number;

    @Attribute(DataTypes.STRING)
    declare nameOfDisease: string;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(1)
    @Max(9)
    declare reasonCode: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Default(1)
    declare numOfDuplicateClaims: number;

    @Attribute(DataTypes.DATEONLY)
    declare treatmentStartDate: Date;

    @Attribute(DataTypes.DATEONLY)
    declare treatmentEndDate: Date;

    @Attribute(DataTypes.STRING)
    declare resultCode: string;

    @Attribute(DataTypes.STRING)
    declare resultName: string;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare validTreatmentDays: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare distanceToHospital: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare codeOfHospital: number;

    @Attribute(DataTypes.STRING)
    declare addrOfHospital: string;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    @Min(10)
    @Max(99)
    declare hospitalCategory: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare licenseNum: number;

    @Attribute(DataTypes.DATEONLY)
    declare datePayment: Date;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costOfClaim: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costGranted: number;

    @Attribute(DataTypes.BOOLEAN)
    declare isActualCost: boolean;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costSelfCharge: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare nonPayment: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costSelfTotal: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costPatientTotal: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare costDiscounted: number;

    @Attribute(DataTypes.INTEGER.UNSIGNED)
    declare numOfTreatment: number;

    @Attribute(DataTypes.FLOAT.UNSIGNED)
    declare ratioOfActual: number;

    @Attribute(DataTypes.BOOLEAN)
    declare isWarnedHospital: boolean;
}

Claim.belongsTo(Contract);

export default Claim;