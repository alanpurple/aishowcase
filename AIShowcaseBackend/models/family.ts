﻿import { DataTypes } from '@sequelize/core';
import sequelize from '../connect-db';
import { Client } from './client';

const Family = sequelize.define('family', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    relationCode: {
        type: DataTypes.INTEGER.UNSIGNED,
        validate: { min: 11, max: 99 }
    }
});

Family.belongsTo(Client, { as: 'client1' });
Family.belongsTo(Client, { as: 'client2' });

export default Family;