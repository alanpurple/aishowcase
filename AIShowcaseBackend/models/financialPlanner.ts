﻿import { DataTypes } from '@sequelize/core';
import sequelize from '../connect-db';

const FinancialPlanner = sequelize.define('financialPlanner', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    statusOfPlanner: DataTypes.STRING,
    dateEmployed: DataTypes.DATEONLY,
    dateFired: DataTypes.DATEONLY,
    previousJob: DataTypes.STRING,
    education: DataTypes.STRING,
    branchCode: DataTypes.INTEGER.UNSIGNED
});

export default FinancialPlanner;