﻿import { Sequelize } from '@sequelize/core';
import { MySqlDialect } from '@sequelize/mysql';
import { User } from './models/user';
import { Client } from './models/client';

//'aishowcasedb', 'root', 'alan',
const connection = new Sequelize({
    host: 'localhost',
    user: 'root',
    password:'alan',
    database: 'aishowcasedb',
    //host: '1.237.79.152',
    dialect: MySqlDialect,
    pool: {
        max: 100,
        min: 0,
        idle: 30000,
        acquire:30000
    },
    models: [Client]
});

export default connection;