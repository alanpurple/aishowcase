﻿import * as passport from 'passport'
import {Strategy as LocalStrategy} from 'passport-local';
import {OAuth2Strategy as GoogleStrategy} from 'passport-google-oauth';
import {Strategy as FacebookStrategy} from 'passport-facebook';

import {User} from './models/user';

passport.serializeUser((user, done) => done(null, user['email']));

passport.deserializeUser(function (em, done) {
    User.findOne({ where: { email: em } }).then(user => {
        done(null, user);
    }).catch(err => done(err));
});

passport.use(new LocalStrategy(
    function (emailAddr, password, done) {
        User.findOne({ where: { email: emailAddr } }).then(user => {
            if (!user) {
                User.create({
                    email: emailAddr,
                    password: password,
                    accountType: 'local'
                }).then(user => done(null, user));
            }
            else
                user.comparePassword(password, done);
        }).catch(err => done(err));
    }
));

passport.use(new GoogleStrategy({
    // local test id and secret
    clientID: '740145020224-mn8fqi8bo920eogb48bcgn17kro526k0.apps.googleusercontent.com',
    clientSecret: 'KdPgeua-gxEl9NR9dpaV2buf',
    callbackURL: 'http://localhost:1337/auth/google/callback'
},
    function (accessToken, refreshToken, profile, done) {
        User.findOne({ where: { oauthId: profile.id } }).then(user => {
            if (user)
                return done(null, user);
            else {
                if (!profile.emails)
                    return done(new Error('cannot recognize email'), user);
                else {
                    return User.create({
                        email: profile.emails[0].value,
                        emailVerified: true,
                        oauthId: profile.id,
                        accountType: 'google'
                    }).then(user => done(null, user));
                }
            }
        }).catch(err => done(err));;
    }
));

passport.use(new FacebookStrategy({
    // local test id and secret
    // these are temporary values
    clientID: '1714693575458575',
    clientSecret: '24411feb5f4e6d8a5c01b85d111afa1a',
    callbackURL: 'http://localhost:1337/auth/facebook/callback',
    profileFields: ['displayName', 'name', 'friendlists', 'email']
},
    function (accessToken, refreshToken, profile, done) {
        User.findOne({ where: { oauthId: profile.id } }).then(user => {
            if (user)
                return done(null, user);
            else {
                if (!profile.emails)
                    return done(new Error('cannot recognize email'), user);
                else {
                    User.create({
                        email: profile.emails[0].value,
                        emailVerified: true,
                        oauthId: profile.id,
                        accountType: 'facebook'
                    }).then(user => done(null, user));
                }
            }
        }).catch(err => done(err));
    }
));

export default passport;