﻿import {Router} from 'express'
const router = Router();
import { Claim } from '../models/claim';

router.get('/', (req, res) => {
    Claim.findAll({
        limit: 100
    }).then(claims => res.send(claims))
        .catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.get('/inspection', (req, res) =>
    require('../ml-services/inspection')(res));

export default router;