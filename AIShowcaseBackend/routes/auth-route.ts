﻿import express = require('express');
const router = express.Router();
import passport = require('passport');

router.get('/google',
    passport.authenticate('google', {
        scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email']
    }));

router.get('/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function (req, res) {
        if (req.user['nickName'])
            res.redirect('/');
        else
            res.redirect('/user-info');
    });

router.get('/facebook',
    passport.authenticate('facebook', {
        scope: ['public_profile', 'user_friends', 'email']
    }));

router.get('/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/login' }),
    function (req, res) {
        if (req.user['nickName'])
            res.redirect('/');
        else
            res.redirect('/user-info');
    });

export default router;