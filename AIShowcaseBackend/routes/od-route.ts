﻿import express = require('express');
const router = express.Router();

const PROTO_PATH = __dirname + '/../../AIShowcaseML/odservice.proto';

import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
import { loadSync } from '@grpc/proto-loader';
const packageDef = loadSync(PROTO_PATH, {
    keepCase: true, longs: String, enums: String, defaults: true, oneofs: true
});
const protoDescriptor = loadPackageDefinition(packageDef);
const odservice = protoDescriptor.odservice['OdService'];

const client = new odservice('localhost:50051',
    credentials.createInsecure());

router.post('/lof', checkParams, (req, res) => {
    client.lof(req.body, (err, result) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (result.error == 0)
            res.sendStatus(401);
        else if (result.error == -1)
            res.send(result.results)
        else
            res.sendStatus(500);
    });
})

router.post('/ee', checkParams, (req, res) => {
    client.ee(req.body, (err, result) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (result.error == 0)
            res.sendStatus(401);
        else if (result.error == -1)
            res.send(result.results)
        else
            res.sendStatus(500);
    });
})

router.post('/isofor', checkParams, (req, res) => {
    client.isofor(req.body, (err, result) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (result.error == 0)
            res.sendStatus(401);
        else if (result.error == -1)
            res.send(result.results)
        else
            res.sendStatus(500);
    });
})

router.post('/ocsvm', checkParams, (req, res) => {
    client.ocsvm(req.body, (err, result) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (result.error == 0)
            res.sendStatus(401);
        else if (result.error == -1)
            res.send(result.results)
        else
            res.sendStatus(500);
    });
})

function checkParams(req, res, next) {
    if (!req.body.tableName || !req.body.attributes
        || req.body.attributes.length < 2)
        res.sendStatus(401);
    else next();
}

export default router;