﻿import {Router} from 'express';
const router = Router();

const PROTO_PATH = __dirname + '/../../AIShowcaseML/lrservice.proto';

import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
import { loadSync } from '@grpc/proto-loader';
const packageDef = loadSync(PROTO_PATH, {
    keepCase: true, longs: String, enums: String, defaults: true, oneofs: true
});
const protoDescriptor = loadPackageDefinition(packageDef);
const lrservice = protoDescriptor.lrservice['LrService'];
const client = new lrservice('localhost:50051',
    credentials.createInsecure());

router.post('/elasticnetcv', checkParams, (req, res) => {
    client.elasticnetcv(req.body, (err, result) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (result.error == 0)
            res.sendStatus(401);
        else if (result.error == -1) {
            delete result.error;
            res.send(result);
        }
        else
            res.sendStatus(500);
    })
});

function checkParams(req, res, next) {
    if (!req.body.tableName || !req.body.sourceColumn || !req.body.targetColumn)
        res.sendStatus(401);
    else
        next();
}

export default router;