﻿import {Router} from 'express';
const router = Router();

const SvmService = require('../ml-services/svmService');

router.post('/sgdclf', (req, res) =>
    SvmService.sgdClf(res, req.body));

router.post('/svclinear', (req, res) =>
    SvmService.svcLinear(res, req.body));

router.post('/svcrbf', (req, res) =>
    SvmService.svcRbf(res, req.body));

router.post('/svcpoly', (req, res) =>
    SvmService.svcPoly(res, req.body));

router.post('qdaclf', (req, res) =>
    SvmService.qdaClf(res, req.body));

export default router;