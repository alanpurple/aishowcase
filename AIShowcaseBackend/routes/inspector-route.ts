﻿import {Router} from 'express';
import { QueryTypes } from '@sequelize/core';
const router = Router();
import sequelize from '../connect-db';
import Inspector from '../ml-services/inspector';

router.get('/tables', (req, res) =>
    sequelize.showAllSchemas({}).then(list => {
        let tableNames=[];
        list.forEach(elem => {
            if (elem['Tables_in_aishowcasedb'] != 'users'
                && elem['Tables_in_aishowcasedb'] != 'sessions'
                && elem['Tables_in_aishowcasedb'] != 'dimreds'
                && elem['Tables_in_aishowcasedb'].indexOf('_dr')<0)
                tableNames.push(elem['Tables_in_aishowcasedb']);
        });
        res.send(tableNames);
    }).catch(err => {
        throw err;
        res.sendStatus(500);
        }));

router.get('/table/:name', (req, res) =>
    sequelize.query('SELECT * FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => res.send(data))
        .catch(err => {
            throw err;
            res.sendStatus(500);
        })
);

router.get('/compact/:name/:attr1/:attr2', (req, res) => {
    sequelize.query('SELECT ' + req.params.attr1 + ',' + req.params.attr2 +
        ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => {
            if (data.length == 0)
                res.sendStatus(404);
            else if (data.length < 400)
                res.send({ data: data, base: 1 });
            else {
                let reducer = Math.floor(data.length / 300);
                let reduced = [];
                data.forEach((elem, index, arr) => {
                    if (index % reducer == 0)
                        reduced.push(elem);
                });
                res.send({ data: reduced, base: reducer });
            }
        }).catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.get('/compact/:name/:attr1/:attr2/:attr3', (req, res) => {
    sequelize.query('SELECT ' + req.params.attr1 + ',' + req.params.attr2 + ',' +
        req.params.attr3 + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => {
            if (data.length == 0)
                res.sendStatus(404);
            else if (data.length < 600)
                res.send({ data: data, base: 1 });
            else {
                let reducer = Math.floor(data.length / 500);
                let reduced = [];
                data.forEach((elem, index, arr) => {
                    if (index % reducer == 0)
                        reduced.push(elem);
                });
                res.send({ data: reduced, base: reducer });
            }
        }).catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.get('/inspect/:name', (req, res) =>
    Inspector(res, req.params.name)
);

// select 3 columns
router.get('/select/:name/:attr1/:attr2/:attr3', (req, res) =>
    sequelize.query('SELECT ' + req.params.attr1 + ',' + req.params.attr2 + ',' +
        req.params.attr3 + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => res.send(data))
        .catch(err => {
            throw err;
            res.sendStatus(500);
        })
);

// select 4 columns
router.get('/select/:name/:attr1/:attr2/:attr3/:attr4', (req, res) =>
    sequelize.query('SELECT ' + req.params.attr1 + ',' + req.params.attr2 + ',' +
        req.params.attr3 + ',' + req.params.attr4 + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => res.send(data))
        .catch(err => {
            throw err;
            res.sendStatus(500);
        })
);

router.get('/3dsample/:name', (req, res) =>
    sequelize.query('SELECT * FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data =>res.send(data))
        .catch(err => {
            throw err;
            res.sendStatus(500);
        })
);

router.get('/2dscatter/:name/:label/:attr1/:attr2', (req, res) => {
    const attr1 = req.params.attr1;
    const attr2 = req.params.attr2;
    const label = req.params.label;
    sequelize.query('SELECT ' + attr1 + ',' + attr2 + ',' + label + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => {
            let result = {};
            data.forEach(elem => {
                if (!(elem[label] in result)) {
                    result[elem[label]] = {};
                }
            });
            for (let prop in result) {
                let filtered = data.filter(elem => elem[label] == prop);
                result[prop][attr1] = filtered.map(elem => elem[attr1]);
                result[prop][attr2] = filtered.map(elem => elem[attr2]);
            }
            res.send(result);
        }).catch(err => {
            throw err;
            res.sendStatus(500);
        })
})

router.get('/3dscatter/:name/:label/:attr1/:attr2/:attr3', (req, res) => {
    const attr1 = req.params.attr1;
    const attr2 = req.params.attr2;
    const attr3 = req.params.attr3;
    const label = req.params.label;
    sequelize.query('SELECT ' + attr1 + ',' + attr2 + ',' + attr3 + ',' +
        label + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => {
            let result = {};
            data.forEach(elem => {
                if (!(elem[label] in result)) {
                    result[elem[label]] = {};
                }
            });
            for (let prop in result) {
                let filtered = data.filter(elem => elem[label] == prop);
                result[prop][attr1] = filtered.map(elem => elem[attr1]);
                result[prop][attr2] = filtered.map(elem => elem[attr2]);
                result[prop][attr3] = filtered.map(elem => elem[attr3]);
            }
            res.send(result);
        }).catch(err => {
            throw err;
            res.sendStatus(500);
        })
})

// temporary 'type' parameter for known column types
router.get('/inspect/:name/:source/:target/:type', (req, res) => {
    sequelize.query('SELECT ' + req.params.source + ',' + req.params.target + ' FROM ' + req.params.name,
        { type: QueryTypes.SELECT })
        .then(data => {
            const src = req.params.source;
            const tgt = req.params.target;
            let result = {};
            //arrSource = [data[0][src]];
            //arrTarget = [data[0][tgt]];
            //for (let i = 1;
            //    i < data.length && (arrSource.length < 51
            //        || arrTarget.length) < 51; i++) {
            //    if (arrSource.indexOf(data[i][src]) == -1)
            //        arrSource.push(data[i][src]);
            //    if (arrTarget.indexOf(data[i][tgt]) == -1)
            //        arrTarget.push(data[i][tgt]);
            //}
            const type = parseInt(req.params.type);
            switch (type) {
                //categorical to categorical
                case 0:
                    // make data for pie charts and bar charts
                    data.forEach(elem => {
                        if (elem[tgt] in result) {
                            if (elem[src] in result[elem[tgt]])
                                result[elem[tgt]][elem[src]]++;
                            else
                                result[elem[tgt]][elem[src]] = 1;
                        }
                        else {
                            result[elem[tgt]] = {};
                            result[elem[tgt]][elem[src]] = 1;
                        }
                    });
                    res.send(result);
                    break;
                //numeric to categorical
                case 1:
                    data.forEach(elem => {
                        if (!(elem[tgt] in result))
                            result[elem[tgt]] = [];
                    });
                    for (let target in result) {
                        let filtered = data.filter(elem => elem[tgt] == target);
                        result[target] = filtered.map(elem => elem[src]);
                    }
                    res.send(result);
                    break;
                default:
                    res.status(400).send('invalid type');
            }
            
            //if (arrSource.length < 50) {
            //    // both Categorical
            //    if (arrTarget.length < 50) {
            //        // make data for pie charts and bar charts
            //        result = {};
            //        data.forEach(elem => {
            //            if (elem[tgt] in result) {
            //                if (elem[src] in result[elem[tgt]])
            //                    result[elem[tgt]][elem[src]]++;
            //                else
            //                    result[elem[tgt]][elem[src]] = 1;
            //            }
            //            else {
            //                result[elem[tgt]] = {};
            //                result[elem[tgt]][elem[src]] = 1;
            //            }
            //        });
            //        res.send(result);
            //    }
            //    else {
            //        res.status(400).send('Cannot set categorical source and numerical target');
            //    }
            //}
            //else {
            //    // numerical to categorical
            //    if (arrTarget.length < 50) {

            //    }
            //    // both numerical
            //    else {

            //    }
            //}
        }).catch(err => {
            throw err;
            res.sendStatus(500);
        });
});

export default router;