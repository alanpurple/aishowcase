﻿import express = require('express');
const router = express.Router();

const User = require('../models/user');

router.get('/', ensureAdmin, function (req, res) {
    User.findAll().then(users => res.send(toJSONArr(users)))
        .catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

function toJSONArr(arr) {
    if (!arr)
        return [];
    else {
        let data = [];
        arr.forEach((value, index, array) => data[index] = value.toJSON());
        return data;
    }
}

function ensureAdmin(req, res, next) {
    if (!req.isAuthenticated())
        res.sendStatus(401);
    else if (req.user.accountType != 'admin')
        res.sendStatus(401);
    else
        next();
}

export default router;