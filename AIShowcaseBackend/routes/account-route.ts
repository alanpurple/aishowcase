﻿import { Router, Request, Response, NextFunction } from 'express';
import passport from 'passport';
import {createTransport} from 'nodemailer';
//import path = require('path');
//import multer = require('multer');
//import fs = require('fs');
//import * as gm from 'gm';

import {User} from '../models/user';

const router = Router();

router.get('/logout', ensureAuthenticated, function (req: Request, res: Response, next: NextFunction) {
    req.logout(err => {
        if (err)
            return next(err);
        else
            res.redirect('/');
    });
});

router.get('/info', function (req, res) {
    if (req.isAuthenticated())
        res.send(req.user);
    else
        res.sendStatus(401);
});

router.get('/checkLogin', function (req, res) {
    let result=false;
    let hasNickName = false;
    if (req.isAuthenticated()) {
        result = true;
        if (req.user['nickName'])
            hasNickName = true;
    }
    res.send({
        result: result,
        hasNickName: hasNickName
    });
});

router.get('/notLoggedIn', function (req, res) {
    if (req.isAuthenticated())
        res.sendStatus(400);
    else
        res.send('not logged in');
});

router.get('/hasNoNick', function (req, res) {
    if (!req.isAuthenticated())
        res.send('notloggedin');
    else if (req.user['nickName'])
        res.send('hasnick');
    else
        res.send('hasnonick');
});

//Authenticate Admin account
router.get('/admin', ensureAuthenticated, function (req, res) {
    if (req.user['accountType'] == 'admin')
        res.sendStatus(200);
    else
        res.sendStatus(401);
});

router.get('/checkNickName/:nickName', function (req, res) {
    User.findOne({ where: { nickName: req.params.nickName } })
        .then(user => {
            if (user)
                res.send('DUPLICATE');
            else
                res.send('AVAILABLE');
        }).catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.get('/checkUser/:id', function (req, res) {
    User.findOne({
        where: { email: decodeURI(req.params.id) }
    }).then(user => {
        if (!user)
            res.sendStatus(404);
        else
            res.send('User already exists')
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

router.put('/', ensureAuthenticated, function (req, res) {
    User.findOne({ where: { email: req.user['email'] } }).then(
        user => {
            if (!user)
                res.sendStatus(404);
            else {
                if (req.body.email) {
                    console.error('email cannot be modified');
                    res.sendStatus(500);
                    return;
                }
                if (req.body.nickName && user.nickName) {
                    console.error('nickName already initialized and cannot be modified');
                    res.sendStatus(500);
                    return;
                }
                user.update(req.body).then(() => res.send('User info updated successfully'))
                    .catch(err => {
                        console.error(err);
                        res.sendStatus(500);
                    });
            }
        }).catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.post('/login',
    function (req, res, next) {
        User.findOne({ where: { email: req.body.username } }).then(user => {
            if (!user)
                res.redirect('/signup');
            else
                next();
        }).catch(err => {
            console.error(err);
            res.redirect('/');
        });
    },
    passport.authenticate('local', { failureRedirect: '/login' }),
    function (req, res) {
        if (!req.user['emailVerified'])
            res.redirect('/emailVerification');
        else if (!req.user['nickName'])
            res.redirect('/user-info');
        //else if (req.user.accountType == 'admin')
        //    res.redirect('/admin');
        else
            res.redirect('/');
    });

router.post('/signup',
    function (req, res, next) {
        User.findOne({ where: { email: req.body.username } }).then(user => {
            if (!user)
                next();
            else
                res.redirect('/login/' + encodeURI(req.body.username));
        }).catch(err => {
            console.error(err);
            res.redirect('/');
        });
    },
    passport.authenticate('local', {
        successRedirect: '/user-info',
        failureRedirect: '/signup'
    }));

// This parameters should be set properly for email-send function
/* const USERNAME = 'wsoh@wise.co.kr';
//Dummy data
const CLIENT_ID = '000000';
const CLIENT_SECRET = 'aaaaa';
const REFRESH_TOKEN = 'not decided'; */
const ACCESS_TOKEN = 'not retreived yet';

/* const generator = require('xoauth2').createXOAuth2Generator({
    user: USERNAME,
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    refreshToken: REFRESH_TOKEN,
    accessToken: ACCESS_TOKEN
}); */

const transporter = createTransport({
    host: 'smtp.gmail.com',
    auth: {
        //xoauth2: generator
        type: 'OAuth2',
        user: 'alanpurple@gmail.com',
        accessToken:ACCESS_TOKEN
    }
});

router.get('/sendVerifyEmail', function (req, res) {
    if (!req.isAuthenticated()) {
        res.sendStatus(401);
        return;
    }
    else if (req.user['accountType'] != 'local' || req.user['emailVerified']) {
        console.log('send verification mail api is for local and unverified accounts');
        res.sendStatus(400);
        return;
    }
    User.findOne({ where: { email: req.user['email'] } }).then(
        user => {
            if (!user) {
                res.sendStatus(404);
                return;
            }
            user.update({
                tempId: Math.floor((Math.random() * 100) + 54),
                dateVerifySent: new Date()
            }).then(() => {
                //link to email verification process page, should be set to actual web service address later
                var link = 'http://' + req.hostname + '/emailVerifyReturn/'
                    + req.user['email'] + '/' + user.tempId;
                transporter.sendMail({
                    from: 'wsoh@wise.co.kr',
                    to: req.user['email'],
                    subject: "이메일 확인.",
                    html: "AIShowcase 계정 확인 메일입니다."
                    + "직접 가입하셨다면 클릭해주십시요.<br><a href=" + link + ">내 계정 활성화</a>"
                }, function (err, info) {
                    if (err) {
                        console.error(err);
                        res.sendStatus(500);
                    }
                    else
                        res.send('message sent');
                });
            }).catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
        }
    ).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

function ensureAuthenticated(req: Request, res: Response, next: NextFunction) {
    if (req.isAuthenticated())
        next();
    else res.sendStatus(401);
}

export default router;