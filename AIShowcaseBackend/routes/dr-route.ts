﻿import express = require('express');
const router = express.Router();
import sequelize from '../connect-db';
import protoLoader = require('@grpc/proto-loader');
import {DimRed} from '../models/dimRed';

const PROTO_PATH = __dirname + '/../../AIShowcaseML/drservice.proto';

import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
import { loadSync } from '@grpc/proto-loader';
const packageDef = loadSync(PROTO_PATH, {
    keepCase: true, longs: String, enums: String, defaults: true, oneofs: true
});
const protoDescriptor = loadPackageDefinition(packageDef);
const drservice = protoDescriptor.drservice['DrService'];

const client = new drservice('localhost:50051',
    credentials.createInsecure());

router.get('/', (req, res) => {
    if (req.isUnauthenticated()) {
        res.sendStatus(401);
        return;
    }
    DimRed.findAll({ where: { userId: req.user['id'] } })
        .then(data => res.send(data))
        .catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

router.get('/all', (req, res) => {
    if (req.isUnauthenticated()) {
        res.sendStatus(401);
        return;
    }
    else if (req.user['type'] != 'admin') {
        res.sendStatus(401);
        return;
    }
    DimRed.findAll().then(data => res.send(data))
        .catch(err => {
            console.error(err);
            res.sendStatus(500);
        });
});

router.delete('/:id', (req, res) => {
    if (req.isUnauthenticated()) {
        res.sendStatus(401);
        return;
    }
    DimRed.findByPk(req.params.id).then(data => {
        if (!data) {
            res.sendStatus(404);
            return;
        }
        if (data.userId != req.user['id'] && req['accountType'] != 'admin') {
            res.sendStatus(401);
            return;
        }
        sequelize.query('DROP TABLE ' + data.tableName)
            .then(() => data.destroy()).then(() => res.send('Deletion Succeeded'))
            .catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
    });
});

router.post('/ldaeigen', (req, res) => {
    if (!req.body.tableName || !req.body.labelColumn) {
        res.sendStatus(401);
        return;
    }
    DimRed.create({
        tableName: 'temp',
        origin: req.body.tableName,
        type: 'LDA eigen',
        label: req.body.labelColumn,
        userId: req.user['id']
    }).then(dr => {
        dr.tableName = dr.origin + '_' + dr.id + '_dr';
        let format = {
            tableName: req.body.tableName, labelColumn: req.body.labelColumn,
            newTableName: dr.tableName
        };
        if (req.body.nComponents)
            format['nComponents'] = req.body.nComponents;
        client.ldaEigen(format, (err, result) => {
            if (err || result.error > -1) {
                dr.destroy();
                console.error(err);
                res.sendStatus(500);
            }
            dr.save().then(() => res.send({
                generated: dr.tableName,
                varianceRatio: result.varianceRatio
            })).catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
        })
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

router.post('/ldasvd', (req, res) => {
    if (!req.body.tableName || !req.body.labelColumn) {
        res.sendStatus(401);
        return;
    }
    DimRed.create({
        tableName: 'temp',
        origin: req.body.tableName,
        type: 'LDA svd',
        label: req.body.labelColumn,
        userId: req.user['id']
    }).then(dr => {
        dr.tableName = dr.origin + '_' + dr.id + '_dr';
        let format = {
            tableName: req.body.tableName, labelColumn: req.body.labelColumn,
            newTableName: dr.tableName
        };
        if (req.body.nComponents)
            format['nComponents'] = req.body.nComponents;
        client.ldaSvd(format, (err, result) => {
            if (err || result.error > -1) {
                dr.destroy();
                console.error(err);
                res.sendStatus(500);
            }
            dr.save().then(() => res.send({
                generated: dr.tableName,
                varianceRatio: result.varianceRatio
            })).catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
        })
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

router.post('/pca', (req, res) => {
    if (!req.body.tableName) {
        res.sendStatus(401);
        return;
    }
    DimRed.create({
        tableName: 'temp',
        origin: req.body.tableName,
        type: 'PCA',
        userId: req.user['id']
    }).then(dr => {
        dr.tableName = dr.origin + '_' + dr.id + '_dr';
        let format = {
            tableName: req.body.tableName,
            newTableName: dr.tableName
        };
        if (req.body.labelColumn) {
            dr.label = req.body.labelColumn;
            format['labelColumn'] = req.body.labelColumn;
        }
        if (req.body.nComponents)
            format['nComponents'] = req.body.nComponents;
        client.pca(format, (err, result) => {
            if (err || result.error > -1) {
                dr.destroy();
                console.error(err);
                res.sendStatus(500);
            }
            dr.save().then(() => res.send({
                generated: dr.tableName,
                variance: result.variance,
                varianceRatio: result.varianceRatio
            })).catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
        })
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

router.post('/pcarsvd', (req, res) => {
    if (!req.body.tableName || !req.body.nComponents) {
        res.sendStatus(401);
        return;
    }
    DimRed.create({
        tableName: 'temp',
        origin: req.body.tableName,
        type: 'PCA rsvd',
        userId: req.user['id']
    }).then(dr => {
        dr.tableName = dr.origin + '_' + dr.id + '_dr';
        let format = {
            tableName: req.body.tableName,
            newTableName: dr.tableName,
            nComponents:req.body.nComponents
        };
        if (req.body.labelColumn) {
            dr.label = req.body.labelColumn;
            format['labelColumn'] = req.body.labelColumn;
        }
        client.pcaRsvd(format, (err, result) => {
            if (err || result.error > -1) {
                dr.destroy();
                console.error(err);
                res.sendStatus(500);
            }
            dr.save().then(() => res.send({
                generated: dr.tableName,
                variance: result.variance,
                varianceRatio: result.varianceRatio
            })).catch(err => {
                console.error(err);
                res.sendStatus(500);
            });
        })
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

export default router;