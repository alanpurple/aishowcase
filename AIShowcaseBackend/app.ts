﻿import debug from 'debug';
import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import session from 'express-session';
import sqlSession from 'express-mysql-session'
const SqlStore = sqlSession(session);

const app = express();
//import sequelize = require('./connect-db');
import {User} from './models/user';
import {DimRed} from './models/dimRed';

const rootPath = path.join(__dirname, '../AIShowcase/ClientApp/dist/ClientApp');

const options = {
    host: 'localhost',
    port: 3306,
    user: 'alan',
    password: 'alantest',
    /*
    host: '1.237.79.152',
    port: 3306,
    user: 'mltest3',
    password: 'wise1012',
    */
    database: 'aishowcasedb'
};

// Initial settings of default admin for test(alanpurple@gmail.com)
User.sync().then(() =>
    User.findByPk(1)).then(user => {
        if (!user) {
            return User.create({
                id: 1,
                email: 'alan@windingroad.co.kr',
                emailVerified: true,
                nickName: 'alanracer',
                accountType: 'admin',
                password: 'testadmin'
            });
        }
        else
            return;
    });
/*
require('./models/client').sync().then(() =>
    require('./models/family').sync().then(() =>
        require('./models/financialPlanner').sync().then(() =>
            require('./models/contract').sync().then(() =>
                sequelize.showAllSchemas().then(list => {
                    const dataExisting = list.map(obj => obj.Tables_in_aishowcasedb).includes('claims');
                    require('./models/claim').sync().then(() => {
                        if (!dataExisting)
                            require('./hanwha-init');
                    }).catch(err => {
                        throw err;
                    });
                })
            ))));
*/
DimRed.sync().catch(err => {
    console.error(err);
    throw err;
});

const sessionStore = new SqlStore(options);

app.use(session({
    name: 'ai_cookie',
    secret: 'do not need to know',
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));

import passport from './passport-init';

app.use(passport.initialize());
app.use(passport.session());

app.use(favicon('./favicon.ico'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

////////////////////////////////////
// Settings for single-page front-end routing
////////////////////////////////////
app.get(['/', '/summary/?', '/admin/?', '/user-info/?',
    '/signup/?', '/login', '/login/*', '/association/?',
    '/outlier-detection', '/svm-classification', '/info-view',
    '/dim-red'],
    function (req, res) {
        res.sendFile(path.join(rootPath, 'index.html'));
    });
// End of front-end routing
///////////////////////////
app.use(express.static(rootPath, { index: false }));

app.use('/account', require('./routes/account-route').default);
app.use('/auth', require('./routes/auth-route').default);
app.use('/user', require('./routes/user-route').default);
app.use('/claim', require('./routes/claim-route').default);
app.use('/inspector', require('./routes/inspector-route').default);
app.use('/svm', require('./routes/svm-route').default);
app.use('/dr', require('./routes/dr-route').default);
app.use('/od', require('./routes/od-route').default);
app.use('/lr', require('./routes/lr-route').default);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});


interface ExpressError {
    status:number,
    message:string
}
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err:ExpressError, req:express.Request, res:express.Response, next:express.NextFunction) {
        console.error(err);
        res.status(err.status || 500);
        res.send(err.message);
    });
}

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), ()=>
    debug('Express server listening on port ' + server.address().port));
