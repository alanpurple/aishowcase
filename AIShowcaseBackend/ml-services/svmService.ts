﻿const PROTO_PATH = __dirname + '/../../AIShowcaseML/svmservice.proto';
import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
import { loadSync } from '@grpc/proto-loader';
const packageDef = loadSync(PROTO_PATH, {
    keepCase: true, longs: String, enums: String, defaults: true, oneofs: true
});
const protoDescriptor = loadPackageDefinition(packageDef);
const svmservice = protoDescriptor.svmservice['SvmService'];

const client = new svmservice('localhost:50051',
    credentials.createInsecure());

module.exports.svcLinear = function (res, data) {
    client.svcLinear(data, function (err, result) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(result);
    });
};

module.exports.sgdClf = function (res, data) {
    client.sgdClf(data, function (err, result) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(result);
    });
};

module.exports.svcRbf = function (res, data) {
    client.svcRbf(data, function (err, result) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(result);
    });
};

module.exports.svcPoly = function (res, data) {
    client.svcPoly(data, function (err, result) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(result);
    });
};

module.exports.qdaClf = function (res, data) {
    client.qdaClf(data, function (err, result) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(result);
    })
};