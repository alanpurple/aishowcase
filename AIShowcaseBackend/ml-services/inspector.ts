﻿const PROTO_PATH = __dirname + '/../../AIShowcaseML/inspector.proto';

import { Response } from 'express';

import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
import { loadSync } from '@grpc/proto-loader';
const packageDef = loadSync(PROTO_PATH, {
    keepCase: true, longs: String, enums: String, defaults: true, oneofs: true
});
const protoDescriptor = loadPackageDefinition(packageDef);
const inspector=protoDescriptor.inspector['Inspector'];
const client=new inspector('localhost:50051',
    credentials.createInsecure());

export default (clientRes:Response, name:string) =>
    client.getSummary({ tableName: name }, function (err, data) {
        if (err) {
            console.error(err);
            clientRes.sendStatus(500);
        }
        else
            clientRes.send(data.summaries);
    });