__author__='Alan Anderson'

from connect_db import getAll
from sqlalchemy.orm import load_only
import pandas
import numpy

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler,LabelEncoder
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC,LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as qda
import joblib

import svmservice_pb2
import svmservice_pb2_grpc

numOfPoints=300

le=LabelEncoder()

class SvmService(svmservice_pb2_grpc.SvmServiceServicer):
    def SgdClf(self, request, context):
        # currently, 3d is not supported
        assert len(request.attributeColumns)<3
        attc= list(request.attributeColumns)
        lblc=request.labelColumn
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc,lblc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X,y=df.loc[:,attc].to_numpy(),df.loc[:,lblc].to_numpy()
        penalty='elasticnet'
        if request.penalty != '':
            penalty=request.penalty
        sgd=make_pipeline(StandardScaler(),SGDClassifier(alpha=0.01, max_iter=200,penalty=penalty))
        sgd.fit(X,y)
        if request.filename!='':
            joblib.dump(sgd,request.filename)
        xa,ya=df.loc[:,attc[0]],df.loc[:,attc[1]]
        xx,yy=numpy.meshgrid(numpy.linspace(xa.min()-1,xa.max()+1,numOfPoints),
                               numpy.linspace(ya.min()-1,ya.max()+1,numOfPoints))
        axisData=[{'name':attc[0],'data':xx[0].tolist()},
                  {'name':attc[1],'data':yy[:,0].tolist()}]
        zData=le.fit_transform(sgd.predict(numpy.c_[xx.ravel(),yy.ravel()])).tolist()

        return svmservice_pb2.SvcResult(z=zData,attributeAxis=axisData)

    def SvcLinear(self, request, context):
        # currently, 3d is not supported
        assert len(request.attributeColumns)<3
        attc= list(request.attributeColumns)
        lblc=request.labelColumn
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc,lblc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X,y=df.loc[:,attc].to_numpy(),df.loc[:,lblc].to_numpy()
        C=1
        if request.C>0:
            C=request.C
        linsvc=make_pipeline(StandardScaler(),LinearSVC(C=C))
        linsvc.fit(X,y)
        if request.filename!='':
            joblib.dump(linsvc,request.filename)
        xa,ya=df.loc[:,attc[0]],df.loc[:,attc[1]]
        xx,yy=numpy.meshgrid(numpy.linspace(xa.min()-1,xa.max()+1,numOfPoints),
                               numpy.linspace(ya.min()-1,ya.max()+1,numOfPoints))

        axisData=[{'name':attc[0],'data':xx[0].tolist()},
                  {'name':attc[1],'data':yy[:,0].tolist()}]
        zData=le.fit_transform(linsvc.predict(numpy.c_[xx.ravel(),yy.ravel()])).tolist()

        return svmservice_pb2.SvcResult(z=zData,attributeAxis=axisData)

    def SvcPoly(self, request, context):
        # currently, 3d is not supported
        assert len(request.attributeColumns)<3
        attc= list(request.attributeColumns)
        lblc=request.labelColumn
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc,lblc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X,y=df.loc[:,attc].to_numpy(),df.loc[:,lblc].to_numpy()
        C=1
        degree=3
        coef0=1
        if request.C>0:
            C=request.C
        if request.degree>0:
            degree=request.degree
        if request.coef0!=0:
            coef0=request.coef0
        svcpoly=make_pipeline(StandardScaler(),
                              SVC(kernel='poly',degree=degree,coef0=coef0,C=C))
        svcpoly.fit(X,y)
        if request.filename!='':
            joblib.dump(svcpoly,request.filename)
        xa,ya=df.loc[:,attc[0]],df.loc[:,attc[1]]
        xx,yy=numpy.meshgrid(numpy.linspace(xa.min()-1,xa.max()+1,numOfPoints),
                               numpy.linspace(ya.min()-1,ya.max()+1,numOfPoints))
        axisData=[{'name':attc[0],'data':xx[0].tolist()},
                  {'name':attc[1],'data':yy[:,0].tolist()}]
        zData=le.fit_transform(svcpoly.predict(numpy.c_[xx.ravel(),yy.ravel()])).tolist()

        return svmservice_pb2.SvcResult(z=zData,attributeAxis=axisData)

    def SvcRbf(self, request, context):
        # currently, 3d is not supported
        assert len(request.attributeColumns)<3
        attc= list(request.attributeColumns)
        lblc=request.labelColumn
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc,lblc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X,y=df.loc[:,attc].to_numpy(),df.loc[:,lblc].to_numpy()
        C=1
        if request.C>0:
            C=request.C
        svcrbf=make_pipeline(StandardScaler(),SVC(kernel='rbf',C=C))
        svcrbf.fit(X,y)
        if request.filename!='':
            joblib.dump(svcrbf,request.filename)
        xa,ya=df.loc[:,attc[0]],df.loc[:,attc[1]]
        xx,yy=numpy.meshgrid(numpy.linspace(xa.min()-1,xa.max()+1,numOfPoints),
                               numpy.linspace(ya.min()-1,ya.max()+1,numOfPoints))
        axisData=[{'name':attc[0],'data':xx[0].tolist()},
                  {'name':attc[1],'data':yy[:,0].tolist()}]
        zData=le.fit_transform(svcrbf.predict(numpy.c_[xx.ravel(),yy.ravel()])).tolist()

        return svmservice_pb2.SvcResult(z=zData,attributeAxis=axisData)
    def QdaClf(self, request, context):
        # currently, 3d is not supported
        assert len(request.attributeColumns)<3
        attc= list(request.attributeColumns)
        lblc=request.labelColumn
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc,lblc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X,y=df.loc[:,attc].to_numpy(),df.loc[:,lblc].to_numpy()
        qdaclf=make_pipeline(StandardScaler(),qda())
        qdaclf.fit(X,y)
        if request.filename!='':
            joblib.dump(qdaclf,request.filename)
        xa,ya=df.loc[:,attc[0]],df.loc[:,attc[1]]
        xx,yy=numpy.meshgrid(numpy.linspace(xa.min()-1,xa.max()+1,numOfPoints),
                               numpy.linspace(ya.min()-1,ya.max()+1,numOfPoints))
        axisData=[{'name':attc[0],'data':xx[0].tolist()},
                  {'name':attc[1],'data':yy[:,0].tolist()}]
        zData=le.fit_transform(qdaclf.predict(numpy.c_[xx.ravel(),yy.ravel()])).tolist()
        return svmservice_pb2.SvcResult(z=zData,attributeAxis=axisData)