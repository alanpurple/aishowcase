import numpy as np
from sqlalchemy import Column,Integer,Float,create_engine
from sqlalchemy.schema import Table
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from urllib import parse
from sklearn.datasets import make_blobs

server = 'localhost'
database = 'aishowcasedb'
driver = 'MySQL ODBC 8.0 Unicode Driver'
id = 'root'
pwd = 'alan'

numOfFeatures=30

engine=create_engine("mysql+mysqlconnector://{}:{}@{}/{}".format(id,pwd,server,database))
#engine=create_engine('mysql+pyodbc://{}:{}@{}/{}?driver={}'.format(id,pwd,server,database,parse.quote_plus(driver)))

Session = sessionmaker(bind=engine)

session = Session()

Base = declarative_base()

class Cluster1Sample(Base):
    __tablename__='cluster1samples'

    id=Column(Integer,primary_key=True)
    f1=Column(Float)
    f2=Column(Float)
    f3=Column(Float)
    groupIndex=Column(Integer)

X,y=make_blobs(n_samples=1500,cluster_std=[0.8,1.0,1.5,2.5,0.7],n_features=3,centers=5,random_state=300)

X=[np.append(elem,y[idx]) for idx,elem in enumerate(X)]
X=np.array(X)
X_filtered = np.vstack((X[y == 0][:1200], X[y == 1][:700], X[y == 2][:480],X[y==3][:990],X[y==4]))

bulk=[]
for elem in X_filtered:
    temp=Cluster1Sample()
    temp.f1=float(elem[0])
    temp.f2=float(elem[1])
    temp.f3=float(elem[2])
    temp.groupIndex=int(elem[3])
    bulk.append(temp)

Base.metadata.create_all(engine)
q=session.query(Cluster1Sample).limit(10)
if len(q.all()) == 0:
    session.bulk_save_objects(bulk)
    session.commit()