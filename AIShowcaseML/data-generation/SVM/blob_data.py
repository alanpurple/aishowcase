from sklearn.feature_extraction import DictVectorizer
from sklearn.datasets import make_blobs
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, create_engine,Float,Integer,String
from sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.orm import relationship, load_only, sessionmaker
from sqlalchemy.schema import Table
from urllib import parse

server = 'localhost'
database = 'aishowcasedb'
driver = 'MySQL ODBC 8.0 Unicode Driver'
id = 'root'
pwd = 'alan'

engine=create_engine("mysql+mysqlconnector://{}:{}@{}/{}".format(id,pwd,server,database))

Session = sessionmaker(bind=engine)

session = Session()

Base = declarative_base()
col = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t']

class blobdata(Base):
    __table__=Table('blobdatas',Base.metadata,
                  Column('id',Integer,primary_key=True),
                  Column('label', Integer),
                  *[Column('{}'.format(i),Float) for i in col])

X, y = make_blobs(n_samples= 1000, cluster_std=2, centers=3, n_features=20, random_state=0)

a_new =[]

for id, arr in enumerate(X):        
    obj = blobdata(label=int(y[id]))
    for idx, elem in enumerate(arr):         
        setattr(obj,'{}'.format(col[idx]),float(elem))        
    a_new.append(obj)

blobdata.metadata.create_all(engine)
q=session.query(blobdata)
if len(q.all()) == 0:
    session.bulk_save_objects(a_new)
    session.commit()