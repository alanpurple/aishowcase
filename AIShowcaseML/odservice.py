﻿__author__='Alan Anderson'

from connect_db import getAll
from sqlalchemy.orm import load_only
import pandas
import numpy
from sklearn.svm import OneClassSVM
from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor

import odservice_pb2
import odservice_pb2_grpc

rng=numpy.random.RandomState(50)

class OdService(odservice_pb2_grpc.OdServiceServicer):
    def ee(self, request, context):
        conn,session,Base=getAll()
        if request.tableName == '' or request.tableName not in Base.classes:
            return odservice_pb2.OdResponse(error=0)
        if len(request.attributes)<2:
            return odservice_pb2.OdResponse(error=0)
        attc=list(request.attributes)
        fraction=0.1
        if request.contamination > 0:
            if request.contamination > 0.3:
                return odservice_pb2.OdResponse(error=0)
            else:
                fraction=request.contamination
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        clf=EllipticEnvelope(contamination=fraction).fit(df)
        result=clf.predict(df)
        y=[bool(elem<0) for elem in result]
        return odservice_pb2.OdResponse(error=-1,results=y)

    def isofor(self, request, context):
        conn,session,Base=getAll()
        if request.tableName == '' or request.tableName not in Base.classes:
            return odservice_pb2.OdResponse(error=0)
        if len(request.attributes)<2:
            return odservice_pb2.OdResponse(error=0)
        attc=list(request.attributes)
        fraction=0.1
        if request.contamination > 0:
            if request.contamination > 0.3:
                return odservice_pb2.OdResponse(error=0)
            else:
                fraction=request.contamination
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        isofor=IsolationForest(contamination=fraction,random_state=rng).fit(df)
        result=isofor.predict(df)
        y=[bool(elem<0) for elem in result]
        return odservice_pb2.OdResponse(error=-1,results=y)

    def lof(self, request, context):
        conn,session,Base=getAll()
        if request.tableName == '' or request.tableName not in Base.classes:
            return odservice_pb2.OdResponse(error=0)
        if len(request.attributes)<2:
            return odservice_pb2.OdResponse(error=0)
        attc=list(request.attributes)
        fraction=0.1
        if request.contamination > 0:
            if request.contamination > 0.3:
                return odservice_pb2.OdResponse(error=0)
            else:
                fraction=request.contamination
        nbds=20
        if request.num > 0:
            nbds=request.num
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        result = LocalOutlierFactor(contamination=fraction,n_neighbors=nbds).fit_predict(df)
        y=[bool(elem < 0) for elem in result]
        return odservice_pb2.OdResponse(error=-1,results=y)

    def ocsvm(self, request, context):
        conn,session,Base=getAll()
        if request.tableName == '' or request.tableName not in Base.classes:
            return odservice_pb2.OdResponse(error=0)
        if len(request.attributes)<2:
            return odservice_pb2.OdResponse(error=0)
        attc=list(request.attributes)
        nu_val=0.5
        if request.contamination > 0:
            if request.contamination > 0.3:
                return odservice_pb2.OdResponse(error=0)
            else:
                nu_val=0.95*request.contamination+0.05
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(*attc))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        ocs=OneClassSVM(nu=nu_val,random_state=rng).fit(df)
        result=ocs.predict(df)
        y=[bool(elem<0) for elem in result]
        return odservice_pb2.OdResponse(error=-1,results=y)