__author__='Alan Anderson'

from connect_db import getAll
import pandas
from pandas import DataFrame as DF
import numpy

from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

import drservice_pb2
import drservice_pb2_grpc

def makeLabels(n):
    array=[]
    for i in range(n):
        array.append('r'+str(i))
    return array

class DrService(drservice_pb2_grpc.DrServiceServicer):
    def LdaEigen(self, request, context):
        if request.labelColumn!='':
            lblc=request.labelColumn
        else:
            return drservice_pb2.DrResponse(error=0)
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data)
        session.close()
        df=pandas.read_sql(q.statement,conn)
        ydf=df.loc[:,lblc]
        y=ydf.values
        X=df.drop(lblc,1).drop('id',1)
        if request.nComponents==0:
            lda=LDA(solver='eigen',shrinkage='auto')
        else:
            lda=LDA(solver='eigen',shrinkage='auto',n_components=request.nComponents)
        X_new=lda.fit_transform(X,y)
        nn=makeLabels(len(lda.explained_variance_ratio_))
        result=DF(df.loc[:,'id']).join(DF(X_new,columns=nn)).join(DF(ydf))
        result.to_sql(request.newTableName,con=conn)
        return drservice_pb2.DrResponse(error=-1,varianceRatio=lda.explained_variance_ratio_)

    def LdaSvd(self, request, context):
        if request.labelColumn!='':
            lblc=request.labelColumn
        else:
            return drservice_pb2.DrResponse(error=0)
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data)
        session.close()
        df=pandas.read_sql(q.statement,conn)
        ydf=df.loc[:,lblc]
        y=ydf.values
        X=df.drop(lblc,1).drop('id',1)
        if request.nComponents==0:
            lda=LDA(solver='svd')
        else:
            lda=LDA(solver='svd',n_components=request.nComponents)
        X_new=lda.fit_transform(X,y)
        nn=makeLabels(len(lda.explained_variance_ratio_))
        result=DF(df.loc[:,'id']).join(DF(X_new,columns=nn)).join(DF(ydf))
        result.to_sql(request.newTableName,con=conn)
        return drservice_pb2.DrResponse(error=-1,varianceRatio=lda.explained_variance_ratio_)

    def Pca(self, request, context):
        if request.labelColumn!='':
            lblc=request.labelColumn
        if request.nComponents<1:
            nComp='mle'
            solver='full'
        else:
            nComp=request.nComponents
            solver='auto'
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data)
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X=df.drop('id',1)
        if request.labelColumn!='':
            X=X.drop(lblc,1)
            ydf=df.loc[:,lblc]
        pca=PCA(n_components=nComp,svd_solver=solver)
        X_new=pca.fit_transform(X)
        nn=makeLabels(pca.n_components_)
        newdf=DF(X_new,columns=nn)
        result=DF(df.loc[:,'id']).join(newdf)
        if request.labelColumn!='':
            result=result.join(DF(ydf))
        result.to_sql(request.newTableName,con=conn)
        return drservice_pb2.DrResponse(error=-1,variance=pca.explained_variance_,
                                        varianceRatio=pca.explained_variance_ratio_)

    def PcaRsvd(self, request, context):
        if request.labelColumn!='':
            lblc=request.labelColumn
        if request.nComponents<1:
            return drservice_pb2.DrResponse(error=0)
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data)
        session.close()
        df=pandas.read_sql(q.statement,conn)
        X=df.drop('id',1)
        if request.labelColumn!='':
            X=X.drop(lblc,1)
            ydf=df.loc[:,lblc]
        pca=PCA(n_components=request.nComponents,svd_solver='randomized')
        X_new=pca.fit_transform(X)
        nn=makeLabels(pca.n_components_)
        result=DF(df.loc[:,'id']).join(DF(X_new,columns=nn))
        if request.labelColumn!='':
            result=result.join(DF(ydf))
        result.to_sql(request.newTableName,con=conn)
        return drservice_pb2.DrResponse(error=-1,variance=pca.explained_variance_,
                                        varianceRatio=pca.explained_variance_ratio_)