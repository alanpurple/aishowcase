__author__='Alan Anderson'

import grpc
from concurrent import futures
import time

from svmservice import SvmService
from inspector import Inspector
from drservice import DrService
from odservice import OdService
from lrservice import LrService
from svmservice_pb2_grpc import add_SvmServiceServicer_to_server
from inspector_pb2_grpc import add_InspectorServicer_to_server
from drservice_pb2_grpc import add_DrServiceServicer_to_server
from odservice_pb2_grpc import add_OdServiceServicer_to_server
from lrservice_pb2_grpc import add_LrServiceServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
add_InspectorServicer_to_server(Inspector(),server)
add_SvmServiceServicer_to_server(SvmService(),server)
add_DrServiceServicer_to_server(DrService(),server)
add_OdServiceServicer_to_server(OdService(),server)
add_LrServiceServicer_to_server(LrService(),server)

server.add_insecure_port('[::]:50051')
server.start()
try:
  while True:
    time.sleep(_ONE_DAY_IN_SECONDS)
except KeyboardInterrupt:
  server.stop(0)