﻿__author__='Alan Anderson'

from connect_db import getAll
import pandas
import numpy
import datetime

import inspector_pb2
import inspector_pb2_grpc

class Inspector(inspector_pb2_grpc.InspectorServicer):
    def GetSummary(self,request,context):
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data)
        session.close()
        df=pandas.read_sql(q.statement,conn)
        obj = df.describe(include='all').to_dict()
        del obj['id']
        result=[]
        for key,data in obj.items():
            temp={'name':key, 'count':int(data['count'])}
            currentData=df.loc[:,key]

            # normal categorical attribute
            if 'unique' in data and not numpy.isnan(data['unique']):
                temp['type']='categorical'
                temp['unique']=int(data['unique'])
                if not isinstance(data['top'],float):
                    if isinstance(data['top'],datetime.date):
                        temp['top']= data['top'].strftime('%Y-%m-%d %H:%M')
                        temp['type']='datetime'
                    else:
                        temp['top']=data['top']
                if not numpy.isnan(data['freq']):
                    temp['freq']=int(data['freq'])
            # numerical, but needs to be categorical
            elif len(numpy.unique(currentData))<20:
                temp['type']='categorical'
                x,y=numpy.unique(currentData,return_counts=True)
                temp['unique']=len(x)
                temp['freq']=max(y)
                temp['top']=str(x[numpy.argmax(y)])
            # normal numerical
            else:
                temp['type']='numeric'
                temp['q1']=data['25%']
                temp['q2']=data['50%']
                temp['q3']=data['75%']
                temp['mean']=data['mean']
                temp['min']=data['min']
                temp['max']=data['max']
                temp['std']=data['std']
            result.append(temp)
        return inspector_pb2.SummaryReply(summaries=result)