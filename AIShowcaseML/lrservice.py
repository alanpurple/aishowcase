﻿__author__='Alan Anderson'

from connect_db import getAll
from sqlalchemy.orm import load_only
import pandas
import numpy
from sklearn.linear_model import LassoLarsCV,LassoLarsIC,ElasticNetCV,LarsCV

import lrservice_pb2
import lrservice_pb2_grpc

class LrService(lrservice_pb2_grpc.LrServiceServicer):
    def elasticnetcv(self, request, context):
        if request.tableName=='' or request.sourceColumn=='' or request.targetColumn=='':
            return lrservice_pb2.LrResponse(error=0)
        numOfFolds=5
        if request.numOfFolds>0:
            numOfFolds=request.numOfFolds
        conn,session,Base=getAll()
        data=Base.classes[request.tableName]
        q=session.query(data).options(load_only(request.sourceColumn,request.targetColumn))
        session.close()
        df=pandas.read_sql(q.statement,conn)
        x=df.loc[:,request.sourceColumn]
        y=df.loc[:,request.targetColumn]
        regr=ElasticNetCV(cv=numOfFolds)
        xarr=x.values.reshape((len(x),1))
        regr.fit(xarr,y)
        return lrservice_pb2.LrResponse(error=-1,alpha=regr.alpha_,slope=regr.coef_[0],intercept=regr.intercept_)