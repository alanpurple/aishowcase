﻿# AIShowcase

This is the showcase application for fraud detection using machine learning techniques

 - AIShowcase - frontend user web
 - AIShowcaseBackend - backend
 - AIShowcaseML - machine learning API

BaseDB: MySQL 5.7.18, but can be used with other databases,
	also can be migrated to NoSQL like MongoDB(with MongooseJS)

Prerequisite for this application source code
 - Angular 4
 - Node.js 8
 - grpc
 - scikit-learn
 - sequelizejs

 This solution is based on Visual Studio 2017.