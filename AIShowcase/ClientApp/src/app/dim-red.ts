import { Component, OnInit } from '@angular/core';

import { InspectorService } from './inspector.service';
import { ErrorAlert } from './error.alert';
import { SummaryData } from './summary.data';

import { DrRequestData, DrResponseData } from './dr.data';
import {DrService } from './dr.service';
import { MatCard, MatCardHeader, MatCardTitle, MatCardContent } from '@angular/material/card';
import { MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { MatOption } from '@angular/material/core';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatFormField } from '@angular/material/form-field';

@Component({
    templateUrl: './dim-red.html',
    styleUrls: ['./common.layout.css'],
    standalone: true,
    imports: [MatFormField, MatSelect, FormsModule, NgFor, MatOption, MatButton, NgIf, MatInput, MatCard, MatCardHeader, MatCardTitle, MatCardContent]
})
export class DimRed implements OnInit {
    constructor(
        private inspectorService: InspectorService,
        private errorAlert: ErrorAlert,
        private drService: DrService
    ) { }

    summary: SummaryData[];
    reducer: 'LDA svd' | 'LDA eigen' | 'PCA' | 'PCA rsvd';
    readonly reducers: string[] = ['LDA svd', 'LDA eigen', 'PCA', 'PCA rsvd'];
    tableNames: string[];
    result: DrResponseData;
    request: DrRequestData = {
        tableName: null,
        labelColumn: null,
        nComponents: null
    };

    ngOnInit() {
        this.inspectorService.getTables().subscribe(
            data => this.tableNames = data,
            err => this.errorAlert.open(err.message)
        );
    }

    getTableSummary() {
        this.inspectorService.getSummary(this.request.tableName).subscribe(
            data => {
                this.summary = data;
            },
            err => this.errorAlert.open(err.message)
        );
    }

    isNotOkay(reducer) {
        if (reducer == 'LDA svd' || reducer == 'LDA eigen')
            return !this.request.labelColumn;
        else if (reducer == 'PCA rsvd')
            return !this.request.nComponents;
        else
            return false;
    }

    proceed() {
        switch (this.reducer) {
            case 'LDA eigen':
                this.drService.ldaeigen(this.request)
                  .subscribe({
                    next: result => this.result = result,
                    error: err => this.errorAlert.open(err)
                  });
                break;
            case 'LDA svd':
                this.drService.ldasvd(this.request)
                  .subscribe({
                    next: result => this.result = result,
                    error: err => this.errorAlert.open(err)
                  });
                break;
            case 'PCA':
                this.drService.pca(this.request)
                  .subscribe({
                    next: result => this.result = result,
                    error: err => this.errorAlert.open(err)
                  });
                break;
            case 'PCA rsvd':
                this.drService.pcarsvd(this.request)
                  .subscribe({
                    next: result => this.result = result,
                    error: err => this.errorAlert.open(err)
                  });
                break;
        }
    }

    reset() {
        this.summary = null;
        this.request.labelColumn = null;
        this.request.nComponents = null;
        this.reducer = null;
        this.result = null;
    }

    oneInDirty(name) {
        return name == 'claims' || name == 'clients'
            || name == 'contracts' || name == 'families'
            || name == 'financialplanners' || name == 'financialPlanners';
    }
}
