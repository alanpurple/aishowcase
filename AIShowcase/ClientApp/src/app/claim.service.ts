﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataSource } from '@angular/cdk/table';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Claim } from './claim.data';

@Injectable()
export class ClaimService {
    constructor(private http: HttpClient) { }

    //get first 100 claims
    getClaims(): Observable<Claim[]> {
        return this.http.get<Claim[]>('/claim');
    }
    getClaimInspection(): Observable<{}> {
        return this.http.get('/claim/inspection')
            .pipe(
            map(res => {
                let arr = [];
                for (let prop in res) {
                    let temp = { attributeName: prop };
                    for (let deepProp in res[prop])
                        temp[deepProp] = res[prop][deepProp];
                    arr.push(temp);
                }
                return arr;
            }));
    }
}

export class ClaimDataSource extends DataSource<Claim> {
    constructor(private claimService: ClaimService) {
        super();
    }

    connect(): Observable<Claim[]>{
        return this.claimService.getClaims();
    }

    disconnect() { }
}

export class ClaimInspectionDataSource extends DataSource<any>{
    constructor(private claimService: ClaimService) {
        super();
    }

    connect(): Observable<any> {
        return this.claimService.getClaimInspection();
    }

    disconnect() { }
}