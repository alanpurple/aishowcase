﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { DrRequestData, DrResponseData } from './dr.data';

@Injectable()
export class DrService {
    constructor(private http: HttpClient) { }

    ldaeigen(data: DrRequestData): Observable<DrResponseData> {
        return this.http.post<DrResponseData>('/dr/ldaeigen', data);
    }

    ldasvd(data: DrRequestData): Observable<DrResponseData> {
        return this.http.post<DrResponseData>('/dr/ldasvd', data);
    }

    pca(data: DrRequestData): Observable<DrResponseData> {
        return this.http.post<DrResponseData>('/dr/pca', data);
    }

    pcarsvd(data: DrRequestData): Observable<DrResponseData> {
        return this.http.post<DrResponseData>('/dr/pcarsvd', data);
    }

    getData(): Observable<{}[]> {
        return this.http.get<{}[]>('/dr');
    }

    getDataAll(): Observable<{}[]> {
        return this.http.get<{}[]>('/dr/all');
    }

    deleteResult(id: number): Observable<boolean> {
        return this.http.delete('/' + id, { responseType: 'text' })
            .pipe(map(str => true));
    }
}