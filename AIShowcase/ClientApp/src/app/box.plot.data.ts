﻿export class BoxPlotData{
    y: any;
    readonly type = 'box';
    name: string;
    marker: MarkerData = new MarkerData();
    boxpoints: 'all' | false | 'suspectedoutliers' | 'Outliers';
}

class MarkerData{
    color: string = null;
    outliercolor: string = null;
    line: LineData = new LineData();
}

class LineData {
    outliercolor: string = null;
    outlierwidth: number = 2;
}