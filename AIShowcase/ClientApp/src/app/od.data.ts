﻿export interface OdData {
    tableName: string;
    attributes: string[];
    num?: number;
    contamination?: number;
}