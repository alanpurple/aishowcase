import { Component, OnInit } from '@angular/core';

//import { newPlot, purge } from 'plotly.js';

import { Observable } from 'rxjs';

import { ScatterData } from './scatter.data';
import { SummaryData } from './summary.data';
import { ContourTraceData } from './contour.data';
import { PolySvcRequest, SgdRequest, SvcRequest,SvcResultData }
    from './svm.data';
import { SvmService } from './svm.service';
import { InspectorService } from './inspector.service';
import {ErrorAlert } from './error.alert';
import { PlotlyModule, PlotlySharedModule } from 'angular-plotly.js';
import { MatProgressSpinner, MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout/flex';
import { MatRadioButton, MatRadioModule } from '@angular/material/radio';
import { MatCardContent, MatCardModule } from '@angular/material/card';
import { MatExpansionModule, MatExpansionPanel, MatExpansionPanelHeader } from '@angular/material/expansion';
import { MatChip, MatChipsModule } from '@angular/material/chips';
import { CommonModule, NgFor, NgIf, NgSwitch, NgSwitchCase } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';

@Component({
    templateUrl: './svm-classification.html',
    styleUrls: ['./common.layout.css'],
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, MatButtonModule, CommonModule, FormsModule, MatChipsModule,
    MatExpansionModule, MatCardModule, MatRadioModule, FlexModule, MatRadioButton, MatProgressSpinnerModule, PlotlyModule]
})
export class SvmClassification implements OnInit {
    constructor(
        private svmService: SvmService,
        private inspectorService: InspectorService,
        private errorAlert: ErrorAlert
    ) { }
    //@ViewChild('svcplot', { static: false }) svcplot: ElementRef;
    plotData=[];
    plotLayout={title:'sample plot'};
    tableNames: string[];
    readonly PossibleCValues = [0.001, 0.01, 0.1, 1, 10, 100, 1000];
    selectedCValue: number = 1;
    degreeOfPoly: number = 2;
    coef0: number = 0;
    selectedTable: string;
    summary: SummaryData[];
    remainingColumns: string[];
    selectedColumnIndex: number;
    attributes: string[] = [];
    Labels: string[] = [];
    label: string;
    private data: {}[];
    isProcessing: boolean = false;
    readonly Classifiers = ['SGD', 'Linear SVC', 'SVC with RBF kernel', 'SVC with Poly kernel','QDA'];
    selectedClassifier: 'SGD' | 'Linear SVC' | 'SVC with RBF kernel' | 'SVC with Poly kernel' | 'QDA'
    = 'Linear SVC';
    private readonly colors: string[] = ['rgba(93, 164, 214, 0.5)', 'rgba(255, 144, 14, 0.5)',
        'rgba(44, 160, 101, 0.5)', 'rgba(255, 65, 54, 0.5)', 'rgba(207, 114, 255, 0.5)',
        'rgba(127, 96, 0, 0.5)', 'rgba(255, 140, 184, 0.5)', 'rgba(79, 90, 117, 0.5)',
        'rgba(222, 223, 0, 0.5)'];
    readonly Penalties = ['l1', 'l2', 'elasticnet'];
    penalty: 'l1' | 'l2' | 'elasticnet' = 'l2';
    optionExpanded: boolean = true;
    ngOnInit() {
      this.inspectorService.getTables().subscribe({
        next: data => this.tableNames = data,
        error: err => this.errorAlert.open(err.message)
      });
    }

    columnsSelected(): boolean {
        if (!this.attributes)
            return false;
        return this.attributes.length > 1 && this.label != null;
    }

    getTableSummary() {
      this.inspectorService.getSummary(this.selectedTable).subscribe({
        next: data => {
          this.summary = data;
          this.resetAttributes();
        },
        error: err => this.errorAlert.open(err.message)
      });
    }

    scatterTraces: ScatterData[];
    contourTrace: ContourTraceData;

    resetIfNeeded() {
        if (this.scatterTraces) {
            this.scatterTraces = null;
            this.plotData=[];
            //purge(this.svcplot.nativeElement);
        }
        this.contourTrace = null;
        this.data = null;
        this.summary = null;
        this.remainingColumns = null;
        this.attributes = null;
        this.Labels = null;
        this.label = null;
        this.optionExpanded = true;
    }

    isNotReady() {
        if (!this.summary || !this.attributes)
            return true;
        return this.attributes.length < 2 || !this.label;
    }

    proceedClassification() {
        if (this.isNotReady())
            return;
        this.isProcessing = true;
        let request;
        this.scatterTraces = [];
        // currently only support attributes of length up to 2
        this.attributes.length = 2;
        let response: Observable<SvcResultData>;
        switch (this.selectedClassifier) {
            case 'SGD':
                request = new SgdRequest();
                request.tableName = this.selectedTable;
                request.attributeColumns = this.attributes;
                request.labelColumn = this.label;
                request.penalty = this.penalty;
                response = this.svmService.SgdClf(request);
                break;
            case 'Linear SVC':
                request = new SvcRequest();
                request.tableName = this.selectedTable;
                request.attributeColumns = this.attributes;
                request.labelColumn = this.label;
                request.C = this.selectedCValue;
                response = this.svmService.SvcLinear(request);
                break;
            case 'SVC with Poly kernel':
                request = new PolySvcRequest();
                request.tableName = this.selectedTable;
                request.attributeColumns = this.attributes;
                request.labelColumn = this.label;
                request.C = this.selectedCValue;
                request.degree = this.degreeOfPoly;
                request.coef0 = this.coef0;
                response = this.svmService.SvcPoly(request);
                break;
            case 'SVC with RBF kernel':
                request = new SvcRequest();
                request.tableName = this.selectedTable;
                request.attributeColumns = this.attributes;
                request.labelColumn = this.label;
                request.C = this.selectedCValue;
                response = this.svmService.SvcRbf(request);
                break;
            case 'QDA':
                request = new SvcRequest();
                request.tableName = this.selectedTable;
                request.attributeColumns = this.attributes;
                request.labelColumn = this.label;
                response = this.svmService.QdaClf(request);
                break;
            default:
                this.errorAlert.open('unknown method: ' + this.selectedClassifier);
        }
      response.subscribe({
        next: data => {
          this.contourTrace = new ContourTraceData();
          let xLen = data.attributeAxis[0].data.length;
          this.contourTrace.x = data.attributeAxis[0].data;
          this.contourTrace.y = data.attributeAxis[1].data;
          for (let i = 0; data.z.length > 0;)
            this.contourTrace.z.push(data.z.splice(0, xLen));
          return this.inspectorService.getScatter(this.selectedTable, this.attributes, this.label)
            .subscribe(
              original => {
                let i = 0;
                for (let prop in original) {
                  let temp = new ScatterData();
                  temp.x = original[prop][data.attributeAxis[0].name];
                  temp.y = original[prop][data.attributeAxis[1].name];
                  temp.mode = 'markers';
                  temp.marker.line.color = this.colors[i++ % 9];
                  temp.name = prop;
                  this.scatterTraces.push(temp);
                }
                let temparr = [];
                Array.prototype.push.apply(temparr, this.scatterTraces);
                temparr.push(this.contourTrace);
                /* newPlot(this.svcplot.nativeElement, temparr,
                    { title: this.selectedClassifier + ' for ' + this.selectedTable }); */
                this.plotData = temparr;
                this.plotLayout.title = this.selectedClassifier + ' for ' + this.selectedTable;
                this.isProcessing = false;
                this.optionExpanded = false;
              }
            );
        },
        error: err => this.errorAlert.open(err.message)
      });

    }

    resetAttributes() {
        if (!this.summary)
            return;
        this.label = null;
        this.Labels = this.summary.filter(elem => elem.type == 'categorical').map(elem => elem.name);
        this.attributes = [];
        this.remainingColumns = this.summary.filter(elem => elem.type == 'numeric').map(elem => elem.name);
        if (this.scatterTraces) {
            this.scatterTraces = null;
            this.plotData=[];
            //purge(this.svcplot.nativeElement);
        }
        this.contourTrace = null;
        this.optionExpanded = true;
    }

    addAttribute() {
        if (!this.remainingColumns || this.remainingColumns.length == 0) {
            console.error('this cant be executed');
            return;
        }
        this.attributes.push(this.remainingColumns.splice(this.selectedColumnIndex, 1)[0]);
        this.selectedColumnIndex = 0;
    }

    oneInDirty(name) {
        return name == 'claims' || name == 'clients'
            || name == 'contracts' || name == 'families'
            || name == 'financialplanners' || name == 'financialPlanners';
    }
}
