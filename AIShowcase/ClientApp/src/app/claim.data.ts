﻿export class Claim {
    id: number|undefined=undefined;
    occpGrpCode1: string='';
    causeOfAccident: string='';
    validTreatmentDays: number=0;
    costOfClaim: number=0;
    costGranted: number=0;
}