import { Component } from '@angular/core';
import { Router, RouterLink, RouterOutlet } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';

import { UserData } from './userData';
import { UserService } from './user.service';
import { ConfirmDialog } from './confirm.dialog';
import { NgIf, NgFor } from '@angular/common';
import { FlexModule } from '@angular/flex-layout/flex';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
    //encapsulation:ViewEncapsulation.ShadowDom
    ,
  standalone: true,
  imports: [FlexModule, NgIf, RouterLink, NgFor, RouterOutlet, MatToolbarModule,
    MatSidenavModule, MatButtonModule, MatIconModule, MatMenuModule, MatListModule]
})
export class AppComponent {
  constructor(
    private _userService: UserService,
    private confirmDialog: ConfirmDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router
  ) {
    iconRegistry.addSvgIcon('google',
      sanitizer.bypassSecurityTrustResourceUrl('icons/google.svg'));
    iconRegistry.addSvgIcon('facebook',
      sanitizer.bypassSecurityTrustResourceUrl('icons/facebook.svg'));
    if (window.outerWidth < 800) {
      this.sidenavMode = 'over';
      this.isOpened = false;
    }
  }
  title = 'AI Showcase App';

  isOpened: boolean = true;
  sidenavMode: 'side' | 'over' | 'push' = 'side';
  msgs:string[];

  confirmMsg(i:number){
    console.log(i);
  }

  handleMenu(sidemenu) {
    if (window.outerWidth < 800)
      sidemenu.close();
  }

  user: UserData;

  ngOnInit() {
    this._userService.getUser()
      .subscribe(
        user => {
          this.user = user;
          if (!user.nickName)
            this.router.navigate(['/user-info']);
        },
        error => {
          if (error.status != 401)
            console.error(error._body);
        });
  }
}
