import { Routes} from '@angular/router';

import { LoggedIn, NotLoggedIn, IsAdmin, HasNick } from './check.login';

import { Login } from './login';
import { Signup } from './signup';
import { Home } from './home';
import { Admin } from './admin';
import { Summary } from './summary';
import { UserInfo } from './user-info';
import { Association } from './association';
import { OutlierDetection } from './outlier-detection';
import { SvmClassification } from './svm-classification';
import { DimRed } from './dim-red';
import { InfoView } from './info-view';

export const routes: Routes = [
  {
    path: '', component: Home,
    canActivate: [HasNick]
  },
  {
    path: 'login', component: Login,
    canActivate: [NotLoggedIn]
  },
  {
    path: 'login/:id', component: Login,
    canActivate: [NotLoggedIn],
    canDeactivate: [LoggedIn]
  },
  {
    path: 'signup', component: Signup,
    canActivate: [NotLoggedIn]
  },
  {
    path: 'user-info', component: UserInfo
  },
  {
    path: 'admin', component: Admin,
    canActivate: [IsAdmin]
  },
  {
    path: 'summary', component: Summary,
    canActivate: [LoggedIn]
  },
  {
    path: 'association', component: Association,
    canActivate: [LoggedIn]
  },
  {
    path: 'outlier-detection', component: OutlierDetection,
    canActivate: [LoggedIn]
  },
  {
    path: 'svm-classification', component: SvmClassification,
    canActivate: [LoggedIn]
  },
  {
    path: 'dim-red', component: DimRed,
    canActivate: [LoggedIn]
  },
  {
    path: 'info-view', component: InfoView
  }
];
