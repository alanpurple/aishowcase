import { Component, OnInit} from '@angular/core';

//import { newPlot } from 'plotly.js';

import { ErrorAlert } from './error.alert';
import { InspectorService } from './inspector.service';

import { ScatterData } from './scatter.data';
import { PlotlySharedModule } from 'angular-plotly.js';

@Component({
    templateUrl: './home.html',
    styleUrls: ['./home.css']
    //encapsulation:ViewEncapsulation.ShadowDom
    ,
    standalone: true,
    imports: [PlotlySharedModule]
})
export class Home implements OnInit {
    constructor(
        private inspectorService: InspectorService,
        private errorAlert: ErrorAlert
    ) { }
    //@ViewChild('3dsample', { static: false }) sample: ElementRef;

    isProcessing: boolean = true;

    trace1: ScatterData = {
        x: [], y: [], z: [],
        mode: 'markers',
        name: 'trace1',
        marker: {
            size: 12,
            line: {
                color: 'rgba(217, 217, 217, 0.14)',
                width: 0.5
            },
            opacity: 0.8
        },
        type: 'scatter3d'
    };

    trace2: ScatterData = {
        x: [], y: [], z: [],
        mode: 'markers',
        name: 'trace2',
        marker: {
            color: 'rgb(127, 127, 127)',
            size: 12,
            symbol: 'circle',
            line: {
                color: 'rgb(204, 204, 204)',
                width: 1
            },
            opacity: 0.8
        },
        type: 'scatter3d'
    };

    layout = {
        margin: {
            l: 0,
            r: 0,
            b: 0,
            t: 0
        }
    };

    plotData:ScatterData[]=[];

  ngOnInit() {
    this.inspectorService.get3dSample('3dscattersamples')
      .subscribe({
        next: data => {
          this.trace1.x = data.map(elem => elem.x1);
          this.trace1.y = data.map(elem => elem.y1);
          this.trace1.z = data.map(elem => elem.z1);
          this.trace2.x = data.map(elem => elem.x2);
          this.trace2.y = data.map(elem => elem.y2);
          this.trace2.z = data.map(elem => elem.z2);
          //newPlot(this.sample.nativeElement, [this.trace1, this.trace2], this.layout);
          this.plotData = [this.trace1, this.trace2];
          this.isProcessing = false;
        },
        error: err => this.errorAlert.open(err)
      })
  }
}
