import { Component, OnInit } from '@angular/core';
import { Color, BarChartModule, PieChartModule } from '@swimlane/ngx-charts';

import { ErrorAlert } from './error.alert';
import { InspectorService } from './inspector.service';
import { LrService } from './lr.service';
import { SummaryData } from './summary.data';

import { BoxPlotData } from './box.plot.data';
import { ScatterData } from './scatter.data';
import { PlotlySharedModule } from 'angular-plotly.js';
import { MatGridList, MatGridTile, MatGridTileText, MatGridTileFooterCssMatStyler } from '@angular/material/grid-list';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { MatButton } from '@angular/material/button';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatOption } from '@angular/material/core';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatFormField } from '@angular/material/form-field';

@Component({
    templateUrl: './association.html',
    standalone: true,
    imports: [MatFormField, MatSelect, FormsModule, NgFor, MatOption, NgIf, MatProgressSpinner, MatButton, MatSlideToggle, BarChartModule, MatGridList, MatGridTile, PieChartModule, MatGridTileText, MatGridTileFooterCssMatStyler, PlotlySharedModule]
})
export class Association implements OnInit {
    constructor(
        private inspectorService: InspectorService,
        private lrService: LrService,
        private errorAlert: ErrorAlert
    ) { }

    tableNames: string[];

    plotData=[];
    plotLayout={title:'sample plot'};
    isPlotly:boolean=false;

    selectedTable: string;
    summary: SummaryData[] = null;
    source: SummaryData;
    target: SummaryData;

    private readonly props = ['name', 'count', 'type', 'unique', 'freq', 'top',
        'mean', 'std', 'min', 'q1', 'q2', 'q3', 'max'];

  readonly colorScheme: Color = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  } as Color;

    private readonly colors: string[] = ['rgba(93, 164, 214, 0.5)', 'rgba(255, 144, 14, 0.5)',
        'rgba(44, 160, 101, 0.5)', 'rgba(255, 65, 54, 0.5)', 'rgba(207, 114, 255, 0.5)',
        'rgba(127, 96, 0, 0.5)', 'rgba(255, 140, 184, 0.5)', 'rgba(79, 90, 117, 0.5)',
        'rgba(222, 223, 0, 0.5)'];

    ngOnInit() {
        this.inspectorService.getTables()
          .subscribe({
            next: list => this.tableNames = list,
            error: err => this.errorAlert.open(err)
          });
    }

    gettingData: boolean = false;

    getSummary() {
        this.summary = null;
        this.gettingData = true;
        this.inspectorService.getSummary(this.selectedTable)
          .subscribe({
            next: data => {
              this.gettingData = false;
              this.summary = data;
            },
            error: err => this.errorAlert.open(err)
          });
        this.source = null;
        this.target = null;
        this.resetAssociation();
    }

    resetAssociation() {
        if (this.associationData) {
            this.isPlotly=false;
            this.plotData=[];
            this.plotLayout={title:''}
            this.associationData = [];
            this.target = null;
        }
        this.type = null;
    }

    resetOnlyAsc() {
        this.associationData = [];
        this.type = null;
    }

    // only category to category case is implemented currently
    isValid(target: SummaryData): boolean {
        if (this.source == target)
            return false;
        if (this.source.type == 'categorical') {
            if (target.type == 'numeric')
                return false;
        }
        if (target.type == 'categorical')
            if (target.unique > 20)
                return false;
        return true;
    }

    associationData: any[] = [];

    graphType: boolean = false;
    type: number = null;
    
    analyzeAssociation() {
        let type=2;
        if (this.source.type == 'numeric') {
            if (this.target.type == 'categorical')
                type = 1;
        }
        else if (this.target.type == 'numeric') {
            console.error('this should not be exist');
            return;
        }
        else
            type = 0;
        if (type == 0 || type == 1) {
            this.inspectorService.getAssociation(
                this.selectedTable, this.source.name, this.target.name, type)
              .subscribe({
                next: data => {
                  if (type == 0) {
                    this.associationData = [];
                    for (let prop in data) {
                      let tempData = { name: prop, series: [] };
                      for (let prop2 in data[prop])
                        tempData.series.push({
                          name: prop2,
                          value: data[prop][prop2]
                        });
                      this.associationData.push(tempData);
                    }
                    this.type = type;
                  }
                  else if (type == 1) {
                    let i = 0;
                    this.associationData = [];
                    for (let prop in data) {
                      let temp = new BoxPlotData();
                      temp.y = data[prop];
                      temp.name = prop;
                      temp.boxpoints = 'suspectedoutliers';
                      temp.marker.color = this.colors[i++ % 9];
                      temp.marker.outliercolor = this.colors[(i + 2) % 9];
                      temp.marker.line.outliercolor = this.colors[(i + 4) % 9];
                      this.associationData.push(temp);
                    }
                    this.type = type;
                    /* newPlot(this.plot.nativeElement, this.associationData,
                        { title: 'boxplots for each target attributes' }); */
                    this.plotData = this.associationData;
                    this.plotLayout.title = 'boxplots for each target attributes';
                    this.isPlotly = true;
                  }
                  else
                    console.error('something is going totally mass');
                }, error: err => this.errorAlert.open(err)
              });
        }
        else if (type == 2) {
            this.lrService.elasticnetcv({
                tableName: this.selectedTable,
                sourceColumn: this.source.name,
                targetColumn: this.target.name
            }).subscribe({
              next: data => {
                let lineTrace = new ScatterData();
                lineTrace.x = [this.source.min - 1, this.source.max + 1];
                lineTrace.y = [(this.source.min - 1) * data.slope + data.intercept,
                (this.source.max + 1) * data.slope + data.intercept];
                lineTrace.name = 'Fitted linear regression';
                lineTrace.mode = 'lines';
                let scatterTrace: ScatterData = new ScatterData();
                scatterTrace.type = 'scatter';
                scatterTrace.name = 'Original Data';
                return this.inspectorService.getDataCompact(
                  this.selectedTable, [this.source.name, this.target.name])
                  .subscribe(original => {
                    scatterTrace.x = original.data.map(elem => elem[this.source.name]);
                    scatterTrace.y = original.data.map(elem => elem[this.target.name]);
                    /* newPlot(this.plot.nativeElement, [lineTrace, scatterTrace],
                        {
                            title: 'linear regression between '
                            + this.source.name + ' and ' + this.target.name
                        }); */
                    this.plotData = [lineTrace, scatterTrace];
                    this.plotLayout.title = 'linear regression between '
                      + this.source.name + ' and ' + this.target.name;
                    this.isPlotly = true;
                  })
              }, error: err => this.errorAlert.open(err.message)
            });
        }
    }

    onSelect(event) {
        console.log(event);
    }
}
