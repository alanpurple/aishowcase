﻿export interface DrRequestData {
    tableName: string;
    labelColumn: string;
    nComponents: number;
}

export interface DrResponseData {
    generated: string;
    variance: number[];
    varianceRatio: number[];
}