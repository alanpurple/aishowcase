import { Component, OnInit } from '@angular/core';
import { Color, LineChartModule } from '@swimlane/ngx-charts';

import { ErrorAlert } from './error.alert';
import { InspectorService, SummaryDataSource } from './inspector.service';
import { SummaryData } from './summary.data';
import { MatTable, MatColumnDef, MatHeaderCellDef, MatHeaderCell, MatCellDef, MatCell, MatHeaderRowDef, MatHeaderRow, MatRowDef, MatRow } from '@angular/material/table';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatButton } from '@angular/material/button';
import { MatOption } from '@angular/material/core';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatFormField } from '@angular/material/form-field';

@Component({
    templateUrl: './summary.html',
    styleUrls: ['./summary.css'],
    standalone: true,
    imports: [MatFormField, MatSelect, FormsModule, NgFor, MatOption, NgIf, MatButton, LineChartModule, MatProgressSpinner, MatTable, MatColumnDef, MatHeaderCellDef, MatHeaderCell, MatCellDef, MatCell, MatHeaderRowDef, MatHeaderRow, MatRowDef, MatRow]
})
export class Summary implements OnInit {
  constructor(
    private inspectorService: InspectorService,
    private errorAlert: ErrorAlert
  ) { }

  tableNames: string[] = [];
  summaries: SummaryData[] = [];

  selectedTable: string = null;

  isn1 = false;

  readonly colorScheme: Color = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  } as Color;

  ngOnInit() {
    this.inspectorService.getTables()
      .subscribe(list => this.tableNames = list,
        err => this.errorAlert.open(err));
    this.summaryDataSource = new SummaryDataSource(this.inspectorService);
  }

  summaryDataSource: SummaryDataSource | null = null;

  props = ['name', 'count', 'type', 'unique', 'freq', 'top',
    'mean', 'std', 'min', 'q1', 'q2', 'q3', 'max'];

  resetInspection() {
    this.summaries = [];
    if (this.selectedTable != 'n1samples') {
      this.isn1 = false;
      this.showGraph = false;
    }
  }

  public processing: boolean = false;

  getSummary() {
    this.processing = true;
    this.summaryDataSource.getSummary(this.selectedTable)
      .subscribe(data => {
        this.summaries = data;
        this.processing = false;
      }, err => this.errorAlert.open(err));
    if (this.selectedTable == 'n1samples')
      this.isn1 = true;
  }

  multi = [{ "name": "최소", "series": [] },
  { "name": "평균", "series": [] },
  { "name": "최대", "series": [] }];
  showGraph = false;

  makeGraph() {
    this.inspectorService.getSummary('n1samples')
      .subscribe(data => {
        data.forEach(elem => {
          this.multi[0].series.push({
            "value": elem.min,
            "name": elem.name
          });
          this.multi[1].series.push({
            "value": elem.mean,
            "name": elem.name
          });
          this.multi[2].series.push({
            "value": elem.max,
            "name": elem.name
          });
        });
        this.showGraph = true;
      }, err => this.errorAlert.open(err))
  }

  clickHandler(data) {
    console.log('Item clicked', data);
  };
}
