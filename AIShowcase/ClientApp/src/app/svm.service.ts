﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { ContourData } from './contour.data';
import { PolySvcRequest, SgdRequest, SvcRequest, SvcResultData }
    from './svm.data';

@Injectable()
export class SvmService {
    constructor(private http: HttpClient) { }

    SgdClf(data: SgdRequest): Observable<SvcResultData> {
        return this.http.post<SvcResultData>('/svm/sgdclf', data);
    }

    SvcLinear(data: SvcRequest): Observable<SvcResultData> {
        return this.http.post<SvcResultData>('/svm/svclinear', data);
    }

    SvcRbf(data: SvcRequest): Observable<SvcResultData> {
        return this.http.post<SvcResultData>('/svm/svcrbf', data);
    }

    SvcPoly(data: PolySvcRequest): Observable<SvcResultData> {
        return this.http.post<SvcResultData>('/svm/svcpoly', data);
    }

    QdaClf(data: SvcRequest): Observable<SvcResultData> {
        return this.http.post<SvcResultData>('/svm/svclinear', data);
    }
}