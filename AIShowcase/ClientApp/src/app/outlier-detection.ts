import { Component, OnInit } from '@angular/core';
import { MatSliderModule, MatSliderDragEvent } from '@angular/material/slider';

//import { newPlot, purge } from 'plotly.js';

import { InspectorService } from './inspector.service';
import { OdService } from './od.service';
import { ErrorAlert } from './error.alert';
import { ConfirmDialog } from './confirm.dialog';
import { ScatterData } from './scatter.data';
import { SummaryData } from './summary.data';

import { OdData } from './od.data';
import { PlotlyModule } from 'angular-plotly.js';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule, MatExpansionPanel, MatExpansionPanelHeader } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';

@Component({
    templateUrl: './outlier-detection.html',
    styleUrls: ['./common.layout.css'],
  standalone: true,
  imports: [MatFormFieldModule, FormsModule, MatExpansionModule, PlotlyModule, CommonModule,
    MatSelectModule, MatTableModule, MatProgressSpinnerModule, MatChipsModule, MatCheckboxModule, MatCardModule,
    MatButtonModule, MatSliderModule]
})
export class OutlierDetection implements OnInit {
    constructor(
        private inspectorService: InspectorService,
        private odService: OdService,
        private errorAlert: ErrorAlert,
        private confirmDialog: ConfirmDialog
    ) {
        this.Ods = [{ name: 'Elliptic Envelop', method: this.odService.ee.bind(this.odService) },
            { name: 'Isolation Forest', method: this.odService.isofor.bind(this.odService) },
            { name: 'One Class SVM', method: this.odService.ocsvm.bind(this.odService) },
            { name: 'Local Outlier Factor', method: this.odService.lof.bind(this.odService) }]
    }
    //@ViewChild('odplot', { static: false }) odplot: ElementRef;
    plotData=[];

    isProcessing: boolean = false;
    gettingTable: boolean = false;

    inliers: ScatterData;
    outliers: ScatterData;
    tableNames: string[];
    selectedTable: string;
    data: {}[];
    summary: SummaryData[];
    hasContamination: boolean = false;
    requestData: OdData = { tableName: null, attributes: [] };
    remainingColumns: string[];
    selectedColumnIndex: number;
    Ods: { name: string, method: any }[];
    selectedOdIndex: number = 0;
    base: number;
    optionExpanded: boolean = true;

    layout = {
        margin: { l: 0, r: 0, b: 0, t: 0 }
    };

    ngOnInit() {
        this.inspectorService.getTables()
            .subscribe(list => this.tableNames = list,
            err => this.errorAlert.open(err.message));
    }

    getTableSummary() {
        this.gettingTable = true;
      this.inspectorService.getSummary(this.requestData.tableName).subscribe({
        next: data => {
          this.summary = data;
          this.resetAttributes();
          this.gettingTable = false;
        },
        error: err => this.errorAlert.open(err.message)
      });
    }

    resetIfNeeded() {
        this.requestData.attributes = [];
        this.summary = null;
        if (this.inliers) {
            this.inliers = null;
            this.outliers = null;
            //purge(this.odplot.nativeElement);
            this.plotData=[];
        }
        if (this.hasContamination) {
            this.hasContamination = false;
            delete this.requestData.contamination;
        }
        if (this.hasNum) {
            this.hasNum = false;
            delete this.requestData.num;
        }
    }

    hasNum: boolean = false;

  numOfNeighbors: number = 20;

  contam?: number;
  numNum?: number;

    numcheck() {
        if (!this.hasNum)
            delete this.requestData.num;
    }

    initData() {
        this.inliers = this.outliers = null;
        this.inspectorService.getSummary(this.selectedTable)
          .subscribe({
            next: data => this.summary = data,
            error: err => this.errorAlert.open(err.message)
          });
    }

    ctcheck() {
        if (!this.hasContamination)
            delete this.requestData.contamination;
    }

    resetAttributes() {
        if (!this.summary)
            return;
        this.requestData.attributes = [];
        this.remainingColumns = this.summary.filter(elem => elem.type == 'numeric').map(elem => elem.name);
        if (this.inliers) {
            this.inliers = this.outliers = null;
            //purge(this.odplot.nativeElement);
            this.plotData=[];
        }
        this.optionExpanded = true;
    }

    getOdResult() {
        if (!this.requestData.tableName || this.requestData.attributes.length < 2) {
            this.errorAlert.open('not ready for outlier-detection');
            return;
        }
        else if (this.requestData.attributes.length > 3) {
            this.confirmDialog
                .open('sorry dimension more than 3 is available for processing but not ready for display');
        }
        this.isProcessing = true;
        this.Ods[this.selectedOdIndex].method(this.requestData)
            .subscribe(marks =>
                this.inspectorService.getDataCompact(
                    this.requestData.tableName,
                    this.requestData.attributes)
                .subscribe({
                  next: data => {
                    const inliers = data.data.filter(
                      (elem, index) => !marks[index * data.base]);
                    const outliers = data.data.filter(
                      (elem, index) => marks[index * data.base]);
                    this.inliers = new ScatterData();
                    this.outliers = new ScatterData();
                    this.inliers.x
                      = inliers.map(elem => elem[this.requestData.attributes[0]]);
                    this.inliers.y
                      = inliers.map(elem => elem[this.requestData.attributes[1]]);
                    this.outliers.x
                      = outliers.map(elem => elem[this.requestData.attributes[0]]);
                    this.outliers.y
                      = outliers.map(elem => elem[this.requestData.attributes[1]]);
                    this.inliers.name = 'inlier';

                    this.outliers.marker['line']['color'] = 'rgb(204, 204, 204)';

                    this.outliers.name = 'outlier';
                    if (this.requestData.attributes.length == 3) {
                      this.inliers.z
                        = inliers.map(elem => elem[this.requestData.attributes[2]]);
                      this.outliers.z
                        = outliers.map(elem => elem[this.requestData.attributes[2]]);
                      this.inliers.type = this.outliers.type = 'scatter3d';
                    }
                    else
                      this.inliers.type = this.outliers.type = 'scattergl';
                    /* newPlot(this.odplot.nativeElement,
                        [this.inliers, this.outliers], this.layout); */
                    this.plotData = [this.inliers, this.outliers];
                    this.isProcessing = false;
                    this.optionExpanded = false;
                  }, error: err => this.errorAlert.open(err.message)
                }));
    }

    isNotReady() {
        if (!this.summary || !this.requestData.attributes)
            return true;
        return this.requestData.attributes.length < 2;
    }

    addAttribute() {
        if (!this.remainingColumns || this.remainingColumns.length == 0) {
            console.error('this cant be executed');
            return;
        }
        this.requestData.attributes.push(this.remainingColumns.splice(this.selectedColumnIndex, 1)[0]);
        this.selectedColumnIndex = 0;
    }

    oneInDirty(name) {
        return name == 'claims' || name == 'clients'
            || name == 'contracts' || name == 'families'
            || name == 'financialplanners' || name == 'financialPlanners';
    }
}
