﻿import {Component} from '@angular/core';
import { MatAnchor } from '@angular/material/button';
import { MatList, MatListItem } from '@angular/material/list';
import { MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatCardSubtitle } from '@angular/material/card';

@Component({
    templateUrl: './info-view.html',
    styleUrls: ['./info-view.css'],
    standalone: true,
    imports: [MatCard, MatCardHeader, MatCardTitle, MatCardContent, MatList, MatListItem, MatCardSubtitle, MatAnchor]
})
export class InfoView { }