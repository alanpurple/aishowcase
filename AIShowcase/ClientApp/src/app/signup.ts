﻿import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from './user.service';
import { NgIf } from '@angular/common';
import { ExtendedModule } from '@angular/flex-layout/extended';
import { FormsModule } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MatFormField } from '@angular/material/form-field';
import { MatToolbar } from '@angular/material/toolbar';
import { MatCard, MatCardContent } from '@angular/material/card';
import { MatAnchor, MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { FlexModule } from '@angular/flex-layout/flex';

@Component({
    templateUrl: 'signup.html',
    styleUrls: ['signup.css'],
    standalone: true,
    imports: [FlexModule, MatIcon, MatAnchor, MatCard, MatToolbar, MatCardContent, MatFormField, MatInput, FormsModule, MatButton, ExtendedModule, NgIf]
})
export class Signup {
    constructor(
        private router: Router,
        private userService: UserService
    ) { }
    email: string;
    emailChecked: boolean;
    password: string;
    passwordConfirm: string;

    checkEmail(): void {
        this.userService.checkuser(this.email)
            .subscribe(
            msg => this.router.navigate(['/login', encodeURI(this.email)]),
            err => {
                if (err.status == 404) {
                    console.info('등록 가능 계정');
                    this.emailChecked = true;
                }
            });
    }

    resetEmail(): void {
        this.emailChecked = false;
        this.email = '';
        this.password = '';
        this.passwordConfirm = '';
    }
}