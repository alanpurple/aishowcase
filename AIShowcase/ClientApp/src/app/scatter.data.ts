﻿export class ScatterData {
    x: number[];
    y: number[];
    z?: number[];
    name: string;
    mode: "lines" | "markers" | "text" | "lines+markers" | "text+markers" | "text+lines" | "text+lines+markers" | "none"
    = 'markers';
    marker: any = {
        size: 9,
        line: {
            color: 'rgba(217, 217, 217, 0.14)',
            width: 0.5
        },
        opacity: 0.8
    };
    type: 'bar' | 'scatter' | 'scattergl' | 'scatter3d' = 'scatter';
}
