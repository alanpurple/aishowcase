import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { UserService } from './user.service';
import { ErrorAlert } from './error.alert';
import { UserData } from './userData';
import { MatButton } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MatFormField } from '@angular/material/form-field';
import { MatCard, MatCardContent } from '@angular/material/card';

@Component({
    templateUrl: './user-info.html',
    standalone: true,
    imports: [MatCard, MatCardContent, MatFormField, MatInput, FormsModule, MatButton]
})
export class UserInfo implements OnInit {
    constructor(
        private dialog: MatDialog,
        private errorAlert: ErrorAlert,
        private router: Router,
        private userService: UserService
    ) { }

    user: UserData;
    nickName: string;

    ngOnInit() {
      this.userService.getUser().subscribe({
        next: user => {
          this.user = user;
          if (user.nickName) {
            this.nickName = user.nickName;
            this.dialog.open(ComingSoonDialog).afterClosed()
              .subscribe(() => this.router.navigate(['/']));
          }
        }, error: err => this.errorAlert.open(err.message)
      });
    }

    checkNick() {
        this.userService.checkNick(this.nickName)
            .subscribe(result => {
                if (result) {
                    this.dialog.open(NickNameConfirmDialog, { data: this.nickName })
                        .afterClosed()
                        .subscribe(yes => {
                            console.log(yes);
                            if (yes)
                                this.userService.saveUser({ nickName: this.nickName })
                                  .subscribe({
                                    next: res => this.router.navigate(['/']),
                                    error: err => this.errorAlert.open(err.message)
                                  });
                            else
                                this.nickName = null;
                        });
                }
                else
                    this.dialog.open(NickNameTakenDialog).afterClosed()
                        .subscribe(() => this.nickName = null);
            });
    }
}

@Component({
    template: `
                <h3 mat-dialog-title>Work in progress...</h3>
                <div mat-dialog-content>홈으로 이동합니다.</div>
                <div mat-dialog-actions>
                <button mat-button color="primary" mat-dialog-close>확인</button>
                </div>
              `,
    standalone: true,
    imports: [MatDialogTitle, MatDialogContent, MatDialogActions, MatButton, MatDialogClose]
})
export class ComingSoonDialog {}

@Component({
    template: `
                <h3 mat-dialog-title>Use this nickname({{data}})?</h3>
                <div mat-dialog-content>You can't change your nickname once you've decided
                </div>
                <button mat-button color="accent" [mat-dialog-close]="true">Use this</button>
                <button mat-button color="primary" mat-dialog-close>Choose again</button>
              `,
    standalone: true,
    imports: [MatDialogTitle, MatDialogContent, MatButton, MatDialogClose]
})
export class NickNameConfirmDialog {
    constructor( @Inject(MAT_DIALOG_DATA) public data: string) { }
}

@Component({
    template: `
                <h3 mat-dialog-title>Nickname Already taken</h3>
                <div mat-dialog-content>Choose another nick please</div>
                <div mat-dialog-actions>
                <button mat-button mat-dialog-close>확인</button>
                </div>
              `,
    standalone: true,
    imports: [MatDialogTitle, MatDialogContent, MatDialogActions, MatButton, MatDialogClose]
})
export class NickNameTakenDialog { }
