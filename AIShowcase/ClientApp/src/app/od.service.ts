import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

import { OdData } from './od.data';

@Injectable()
export class OdService {
  constructor(private http: HttpClient) { }

  lof(data: OdData): Observable<boolean[]> {
    if (!data.tableName || !data.attributes || data.attributes.length < 2)
      return throwError('bad input');
    else
      return this.http.post<boolean[]>('/od/lof', data);
  }
  ee(data: OdData): Observable<boolean[]> {
    if (!data.tableName || !data.attributes || data.attributes.length < 2)
      return throwError('bad input');
    else
      return this.http.post<boolean[]>('/od/ee', data);
  }
  isofor(data: OdData): Observable<boolean[]> {
    if (!data.tableName || !data.attributes || data.attributes.length < 2)
      return throwError('bad input');
    else
      return this.http.post<boolean[]>('/od/isofor', data);
  }
  ocsvm(data: OdData): Observable<boolean[]> {
    if (!data.tableName || !data.attributes || data.attributes.length < 2)
      return throwError('bad input');
    else
      return this.http.post<boolean[]>('/od/ocsvm', data);
  }
}
