﻿export class ContourData{
    name: string;
    data: number[];
}

export class ContourTraceData {
    x: number[];
    y: number[];
    z: number[][] = [];
    readonly type = 'contour';
}