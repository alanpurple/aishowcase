﻿import {Component,OnInit,OnDestroy} from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import {Subscription} from 'rxjs';

import {UserService} from './user.service';
import { ExtendedModule } from '@angular/flex-layout/extended';
import { FormsModule } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MatFormField } from '@angular/material/form-field';
import { MatToolbar } from '@angular/material/toolbar';
import { MatCard, MatCardContent } from '@angular/material/card';
import { MatAnchor, MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { FlexModule } from '@angular/flex-layout/flex';

@Component({
    templateUrl: 'login.html',
    styleUrls: ['signup.css'],
    standalone: true,
    imports: [FlexModule, MatIcon, MatAnchor, MatCard, MatToolbar, MatCardContent, MatFormField, MatInput, FormsModule, MatButton, ExtendedModule, RouterLink]
})
export class Login implements OnInit, OnDestroy {
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService
    ) { }

    email: string;
    password: string;
    emailChecked: boolean;
    private sub: Subscription;

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.email = decodeURI(params['id']);
                this.emailChecked = true;
            }
        });
    }

    ngOnDestroy() {
        if (this.sub)
            this.sub.unsubscribe();
    }

    checkEmail() {
        this.userService.checkuser(this.email)
            .subscribe(
            msg => {
                console.info('Valid account');
                this.emailChecked = true;
            },
            err => {
                if (err.status == 404)
                    console.info('no account');
                else
                    console.error(err);
            });
    }

    resetEmail() {
        this.emailChecked = false;
        this.email = '';
        this.password = '';
    }
}