import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { sample3dData } from './sample3d.data';
import { SummaryData } from './summary.data';

@Injectable()
export class InspectorService {
  constructor(private http: HttpClient) { }

  getTables(): Observable<string[]> {
    return this.http.get<string[]>('/inspector/tables');
  }

  getTableData(name: string, attributes?: string[]): Observable<{}[]> {
    if (!attributes)
      return this.http.get<{}[]>('/inspector/table/' + name);
    else if (attributes.length > 4)
      return throwError('currently support up to 3d');
    else if (attributes.length > 2)
      return this.http.get<{}[]>('/inspector/select/' + name + '/'
        + attributes.join('/'));
    else
      return throwError('not enough attributes');
  }

  getDataCompact(name: string, attributes: string[]): Observable<CompactData> {
    if (!attributes || attributes.length < 2)
      return throwError('invalid request');
    else if (attributes.length > 3)
      return throwError('this api is for attributes of length less than 4');
    else
      return this.http.get<CompactData>('inspector/compact/' + name + '/' + attributes.join('/'));
  }

  getScatter(name: string, attributes: string[], label: string): Observable<{}> {
    if (attributes.length < 2 || attributes.length > 3)
      return throwError('attributes with wrong length');
    else if (attributes.length == 2)
      return this.http.get<{}>('/inspector/2dscatter/' + name + '/'
        + label + '/' + attributes.join('/'));
    else
      return this.http.get<{}>('/inspector/3dscatter/' + name + '/'
        + label + '/' + attributes.join('/'));
  }

  getSummary(name:string): Observable<SummaryData[]> {
    if(name==undefined||name==null)
      throwError('name cant be null');
    return this.http.get<SummaryData[]>('/inspector/inspect/' + name)
      .pipe(
        map(data => {
          let filtered = data.filter(elem => elem.type != 'datetime');
          filtered.forEach(row => {
            for (let prop in row)
              switch (prop) {
                case 'mean': case 'min':
                case 'std': case 'q1': case 'q2':
                case 'q3': case 'max':
                  if (row[prop])
                    row[prop] = parseFloat(row[prop].toFixed(3));
              }
          })
          return filtered;
        }));
  }
  get3dSample(name:string): Observable<sample3dData[]> {
    return this.http.get<sample3dData[]>('/inspector/3dsample/' + name);
  }

  // currently support categorical to categorical only
  getAssociation(tableName:string, source:string, target:string, type:number): Observable<{}> {
    return this.http.get<{}>('/inspector/inspect/' + tableName
      + '/' + source + '/' + target + '/' + type);
  }
}

interface CompactData {
  data: {}[];
  base: number;
}

export class SummaryDataSource {
  constructor(
    private inspectorService: InspectorService
    ) { }

  getSummary(name:string):Observable<SummaryData[]>{
    return this.inspectorService.getSummary(name);
  }
}
