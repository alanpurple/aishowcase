import { Component, Injectable, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogTitle, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';

@Component({
    template: `
                <h3 mat-dialog-title>{{data}}</h3>
                <div mat-dialog-actions>
                <button mat-button mat-dialog-close>확인</button>
                </div>
              `,
    standalone: true,
    imports: [MatDialogTitle, MatDialogActions, MatButton, MatDialogClose]
})
export class ConfirmDialogTemplate {
    constructor( @Inject(MAT_DIALOG_DATA) public data: string) { }
}

@Injectable()
export class ConfirmDialog {
    constructor(private dialog: MatDialog) { }

    open(message?: string) {
        this.dialog.open(ConfirmDialogTemplate, {
            data: message ? message : '확인 메시지입니다'
        });
    }
}
