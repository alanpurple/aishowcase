﻿import { ContourData } from './contour.data';

export class SvcRequest {
    tableName: string;
    attributeColumns: string[];
    labelColumn: string;
    C: number;
}

export class PolySvcRequest extends SvcRequest {
    constructor() { super(); }
    degree: number;
    coef0: number;
}

export class SgdRequest {
    tableName: string;
    attributeColumns: string[];
    labelColumn: string;
    penalty: string;
}

export class SvcResultData {
    z: number[];
    attributeAxis: ContourData[];
}