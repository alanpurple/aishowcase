import { Component, Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';

@Component({
    template: `
                <h3 mat-dialog-title>{{data}}</h3>
                <mat-dialog-content>홈으로 이동합니다.</mat-dialog-content>
                <div mat-dialog-actions>
                <button mat-button mat-dialog-close>확인</button>
                </div>
              `,
    standalone: true,
    imports: [MatDialogTitle, MatDialogContent, MatDialogActions, MatButton, MatDialogClose]
})
export class ErrorDialog {
    constructor( @Inject(MAT_DIALOG_DATA) public data: string) { }
}

@Injectable()
export class ErrorAlert {
    constructor(private dialog: MatDialog,
        private router: Router) {
    }

    open(msg?: string) {
        this.dialog.open(ErrorDialog, {
            data: msg ? msg : '에러남!'
        }).afterClosed()
            .subscribe(() => this.router.navigate(['/']));
    }
}
